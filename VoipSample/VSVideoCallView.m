//
//  VSVideoCallView.m
//  VoipSample
//
//  Created by Dmitry Lyzlov on 08/03/16.
//  Copyright © 2016 Dmitry Lyzlov. All rights reserved.
//

#import "VSVideoCallView.h"

#import <AVFoundation/AVFoundation.h>

#import <RTCEAGLVideoView.h>
#import <RTCVideoTrack.h>

@interface VSVideoCallView () < RTCEAGLVideoViewDelegate >

@property (strong, nonatomic) RTCEAGLVideoView *remoteVideoView;
@property (strong, nonatomic) RTCVideoTrack *remoteVideoTrack;
// TODO: add local video view

@property (assign, nonatomic) CGSize remoteVideoViewSize;

@end

@implementation VSVideoCallView

- (void)setRemoteVideoTrack:(RTCVideoTrack *)remoteVideoTrack
{
    if (_remoteVideoTrack == remoteVideoTrack)
    {
        return;
    }

    RTCEAGLVideoView *remoteVideoView = self.remoteVideoView;
    [_remoteVideoTrack removeRenderer:remoteVideoView];
    _remoteVideoTrack = remoteVideoTrack;
    [remoteVideoView renderFrame:nil];
    [_remoteVideoTrack addRenderer:remoteVideoView];
}

#pragma mark - Public

- (instancetype)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self)
    {
        [self initView];
    }

    return self;
}

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self)
    {
        [self initView];
    }

    return self;
}

- (void)layoutSubviews
{
    CGRect bounds = self.superview.bounds;
    CGSize remoteVideoViewSize = self.remoteVideoViewSize;
    UIView *remoteVideoView = self.remoteVideoView;
    if (remoteVideoViewSize.width > 0 && remoteVideoViewSize.height > 0)
    {
        CGRect remoteVideoViewFrame = AVMakeRectWithAspectRatioInsideRect(remoteVideoViewSize, bounds);
        CGFloat scale = 1;
        if (remoteVideoViewFrame.size.width > remoteVideoViewFrame.size.height)
        {
            scale = bounds.size.height / remoteVideoViewFrame.size.height;
        }
        else
        {
            scale = bounds.size.width / remoteVideoViewFrame.size.width;
        }
        remoteVideoViewFrame.size.height *= scale;
        remoteVideoViewFrame.size.width *= scale;
        remoteVideoView.frame = remoteVideoViewFrame;
        remoteVideoView.center = CGPointMake(CGRectGetMidX(bounds), CGRectGetMidY(bounds));
    }
    else
    {
        _remoteVideoView.frame = bounds;
    }
}


#pragma mark - RTCEAGLVideoViewDelegate

- (void)videoView:(RTCEAGLVideoView *)videoView didChangeVideoSize:(CGSize)size
{
    self.remoteVideoViewSize = size;
    [self setNeedsLayout];
}

#pragma mark - Private

- (void)initView
{
    self.translatesAutoresizingMaskIntoConstraints = YES;

    RTCEAGLVideoView *remoteVideoView = [[RTCEAGLVideoView alloc] initWithFrame:CGRectZero];
    remoteVideoView.delegate = self;
    [self addSubview:remoteVideoView];
    self.remoteVideoView = remoteVideoView;
}

@end
