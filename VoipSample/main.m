//
//  main.m
//  VoipSample
//
//  Created by Dmitry Lyzlov on 18/02/16.
//  Copyright © 2016 Dmitry Lyzlov. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "VSAppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([VSAppDelegate class]));
    }
}
