//
//  XMPPIQ+Utilities.m
//  VoipSample
//
//  Created by Dmitry Lyzlov on 05/03/16.
//  Copyright © 2016 Dmitry Lyzlov. All rights reserved.
//

#import "XMPPIQ+Utilities.h"

#import <NSXMLElement+XMPP.h>

NSString * const XMLNS_JINGLE = @"urn:xmpp:jingle:1";
NSString * const XMLNS_JINGLE_RTP = @"urn:xmpp:jingle:apps:rtp:1";
NSString * const XMLNS_JINGLE_RTP_RINGING = @"urn:xmpp:jingle:apps:rtp:1:info";
NSString * const XMLNS_JINGLE_TRANSPORT_UDP_RAW = @"urn:xmpp:jingle:transports:raw-udp:1";
NSString * const XMLNS_JINGLE_TRANSPORT_UDP_ICE = @"urn:xmpp:jingle:transports:ice-udp:1";
NSString * const XMLNS_JINGLE_SDP = @"urn:xmpp:jingle:apps:sdp"; // experimental http://xmpp.org/extensions/inbox/jingle-sdp.html
NSString * const XMLNS_JINGLE_DTLS = @"urn:xmpp:jingle:apps:dtls:0";

NSString * const ELEMENT_JINGLE = @"jingle";
NSString * const ELEMENT_CONTENT = @"content";
NSString * const ELEMENT_DESCRIPTION = @"description";
NSString * const ELEMENT_PAYLOAD_TYPE = @"payload-type";
NSString * const ELEMENT_RINGING = @"ringing";
NSString * const ELEMENT_PARAMETER = @"parameter";
NSString * const ELEMENT_FINGERPRINT = @"fingerprint";
NSString * const ELEMENT_TRANSPORT = @"transport";
NSString * const ELEMENT_CANDIDATE = @"candidate";
NSString * const ELEMENT_REASON = @"reason";
NSString * const ELEMENT_SUCCESS = @"success";
NSString * const ELEMENT_BUSY = @"busy";

NSString * const ATTRIBUTE_FROM = @"from";
NSString * const ATTRIBUTE_ACTION = @"action";
NSString * const ATTRIBUTE_INITIATOR = @"initiator";
NSString * const ATTRIBUTE_RESPONDER = @"responder";
NSString * const ATTRIBUTE_SID = @"sid";
NSString * const ATTRIBUTE_CREATOR = @"creator";
NSString * const ATTRIBUTE_ID = @"id";
NSString * const ATTRIBUTE_NAME = @"name";
NSString * const ATTRIBUTE_MEDIA = @"media";
NSString * const ATTRIBUTE_MAXPTIME = @"maxptime";
NSString * const ATTRIBUTE_PTIME = @"ptime";
NSString * const ATTRIBUTE_CLOCKRATE = @"clockrate";
NSString * const ATTRIBUTE_CHANNELS = @"channels";
NSString * const ATTRIBUTE_VALUE = @"value";
NSString * const ATTRIBUTE_HASH = @"hash";
NSString * const ATTRIBUTE_SETUP = @"setup";
NSString * const ATTRIBUTE_GENERATION = @"generation";
NSString * const ATTRIBUTE_IP = @"ip";
NSString * const ATTRIBUTE_PORT = @"port";
NSString * const ATTRIBUTE_PWD = @"pwd";
NSString * const ATTRIBUTE_UFRAG = @"ufrag";
NSString * const ATTRIBUTE_COMPONENT = @"component";
NSString * const ATTRIBUTE_FOUNDATION = @"foundation";
NSString * const ATTRIBUTE_NETWORK = @"network";
NSString * const ATTRIBUTE_PRIORITY = @"priority";
NSString * const ATTRIBUTE_PROTOCOL = @"protocol";
NSString * const ATTRIBUTE_REL_ADDR = @"rel-addr";
NSString * const ATTRIBUTE_REL_PORT = @"rel-port";
NSString * const ATTRIBUTE_TYPE = @"type";

NSString * const ATTRIBUTE_ACTION_VALUE_INITIATE = @"session-initiate";
NSString * const ATTRIBUTE_ACTION_VALUE_TERMINATE = @"session-terminate";
NSString * const ATTRIBUTE_ACTION_VALUE_ACCEPT = @"session-accept";
NSString * const ATTRIBUTE_ACTION_VALUE_INFO = @"session-info";
NSString * const ATTRIBUTE_CONTENT_NAME_VALUE_AUDIO = @"voice";
NSString * const ATTRIBUTE_CONTENT_NAME_VALUE_VIDEO = @"webcam";

NSString * const ELEMENT_FINGERPRINT_VALUE = @"fingerprint-value";

@implementation XMPPIQ (Utilities)

#pragma mark - Public

- (void)fillElement:(NSXMLElement *)element withValuesFromDictionary:(NSDictionary *)values byKeys:(NSArray *)keys
{
    NSString *value = nil;
    for (NSString *key in keys)
    {
        value = values[key];
        if (value)
        {
            [element addAttributeWithName:key stringValue:value];
        }
    }
}

+ (NSDictionary *)extractJingleRTPDataFromSDPString:(NSMutableString *)SDPString;
{
    /*
     v=0
     o=- 5353582889005784619 2 IN IP4 127.0.0.1
     s=-
     t=0 0
     a=group:BUNDLE audio
     a=msid-semantic: WMS VSMS
     m=audio 9 UDP/TLS/RTP/SAVPF 111 103 104 9 102 0 8 106 105 13 127 126
     c=IN IP4 0.0.0.0
     a=rtcp:9 IN IP4 0.0.0.0
     a=ice-ufrag:BG7iAey9Ma+oUVGB
     a=ice-pwd:WXAijhruZsxVjDmk1GDevVrM
     a=fingerprint:sha-256 BF:8E:C1:07:7D:74:A0:C0:DA:D9:5C:25:BA:F5:AC:C1:47:BB:27:0F:5A:D2:5E:00:78:7A:5F:60:81:00:91:95
     a=setup:actpass
     a=mid:audio
     a=extmap:1 urn:ietf:params:rtp-hdrext:ssrc-audio-level
     a=extmap:3 http://www.webrtc.org/experiments/rtp-hdrext/abs-send-time
     a=sendrecv
     a=rtcp-mux
     a=rtpmap:111 opus/48000/2
     a=fmtp:111 minptime=10; useinbandfec=1
     a=rtpmap:103 ISAC/16000
     a=rtpmap:104 ISAC/32000
     a=rtpmap:9 G722/8000
     a=rtpmap:102 ILBC/8000
     a=rtpmap:0 PCMU/8000
     a=rtpmap:8 PCMA/8000
     a=rtpmap:106 CN/32000
     a=rtpmap:105 CN/16000
     a=rtpmap:13 CN/8000
     a=rtpmap:127 red/8000
     a=rtpmap:126 telephone-event/8000
     a=maxptime:60
     a=ssrc:2229090023 cname:ZUHBmqnz79jJAEWq
     a=ssrc:2229090023 msid:VSMS VSMSa0
     a=ssrc:2229090023 mslabel:VSMS
     a=ssrc:2229090023 label:VSMSa0
     */

    // TODO: add support of all attributes

    NSMutableDictionary *jingleRTPData = [NSMutableDictionary dictionary];
    NSDictionary *mappingAttributes = @{ @"fingerprint" : ATTRIBUTE_HASH,
                                         @"setup" : ATTRIBUTE_SETUP,
                                         @"ice-pwd" : ATTRIBUTE_PWD,
                                         @"ice-ufrag" : ATTRIBUTE_UFRAG };

    NSUInteger matches = 0;
    NSArray *lines = [SDPString componentsSeparatedByCharactersInSet:[NSCharacterSet newlineCharacterSet]];
    for (NSString *item in lines)
    {
        if (item.length == 0)
        {
            continue;
        }

        BOOL found = NO;
        NSRange colonRange = [item rangeOfString:@":"];
        NSUInteger index = colonRange.location + colonRange.length;
        if (colonRange.location != NSNotFound && index < item.length)
        {
            NSString *trimmedSearchAttributeName = [item substringToIndex:colonRange.location];
            trimmedSearchAttributeName = [trimmedSearchAttributeName stringByReplacingOccurrencesOfString:@"a=" withString:@""];
            NSString *attributeName = mappingAttributes[trimmedSearchAttributeName];
            if (attributeName)
            {
                found = YES;
                NSString *value = [item substringFromIndex:index];
                if ([attributeName isEqualToString:ATTRIBUTE_HASH])
                {
                    [jingleRTPData addEntriesFromDictionary:[[self class] formatFingerprintParametersToJingleFromString:value]];
                }
                else
                {
                    [jingleRTPData setObject:value forKey:attributeName];
                }
            }
        }

        if (found)
        {
            matches++;
            // TODO: http://xmpp.org/extensions/inbox/jingle-sdp.html proposed that these lines should be removed... but why?
            //[SDPString stringByReplacingOccurrencesOfString:item withString:@""];
        }

        if (matches == mappingAttributes.count)
        {
            break;
        }
    }

    return jingleRTPData;
}

+ (NSDictionary *)extractICECandidateDataFromSDPString:(NSString *)SDPString
{
    NSMutableDictionary *candidateData = [NSMutableDictionary dictionary];

    /*
     candidate:2608808550 1 udp 2122260223 192.168.1.33 61678 typ host generation 0 ufrag JwfNB+YFLnOJwx0I,
     candidate:2608808550 2 udp 2122260222 192.168.1.33 59971 typ host generation 0 ufrag JwfNB+YFLnOJwx0I,
     candidate:1836006322 2 udp 1686052606 95.37.196.92 59971 typ srflx raddr 192.168.1.33 rport 59971 generation 0 ufrag JwfNB+YFLnOJwx0I,
     candidate:1836006322 1 udp 1686052607 95.37.196.92 61678 typ srflx raddr 192.168.1.33 rport 61678 generation 0 ufrag JwfNB+YFLnOJwx0I,
     candidate:3590110870 1 tcp 1518280447 192.168.1.33 62960 typ host tcptype passive generation 0 ufrag JwfNB+YFLnOJwx0I,
     candidate:3590110870 2 tcp 1518280446 192.168.1.33 62961 typ host tcptype passive generation 0 ufrag JwfNB+YFLnOJwx0I
     */

    NSArray *items = [SDPString componentsSeparatedByCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    NSUInteger index = 0;
    if (items.count > index + 1)
    {
        [candidateData setObject:[(NSString *)items[index] stringByReplacingOccurrencesOfString:@"candidate:" withString:@""] forKey:ATTRIBUTE_FOUNDATION];
    }

    index++;
    NSArray *attributeNames = @[ ATTRIBUTE_COMPONENT, ATTRIBUTE_PROTOCOL, ATTRIBUTE_PRIORITY, ATTRIBUTE_IP, ATTRIBUTE_PORT ];
    for (NSUInteger j = 0; index < items.count && j < attributeNames.count; index++, j++)
    {
        [candidateData setObject:items[index] forKey:attributeNames[j]];
    }

    NSDictionary *attributeMapping = @{  @"typ" : ATTRIBUTE_TYPE,
                                         @"generation" : ATTRIBUTE_GENERATION,
                                         @"raddr" : ATTRIBUTE_REL_ADDR,
                                         @"rport" : ATTRIBUTE_REL_PORT };
    for (; index + 1 < items.count; index += 2)
    {
        NSString *attributeName = attributeMapping[items[index]];
        if (attributeName)
        {
            [candidateData setObject:items[index + 1] forKey:attributeName];
        }
    }

    return candidateData;
}

#pragma mark - Private

+ (NSDictionary *)formatFingerprintParametersToJingleFromString:(NSString *)fingerprintString
{
    NSMutableDictionary *result = [NSMutableDictionary dictionary];

    NSRange rangeOfSpaceSymbol = [fingerprintString rangeOfString:@" "];
    if (rangeOfSpaceSymbol.location != NSNotFound)
    {
        if (rangeOfSpaceSymbol.location < fingerprintString.length)
        {
            [result setObject:[fingerprintString substringToIndex:rangeOfSpaceSymbol.location] forKey:ATTRIBUTE_HASH];
        }
        
        NSUInteger index = rangeOfSpaceSymbol.location + rangeOfSpaceSymbol.length;
        if (index < fingerprintString.length)
        {
            [result setObject:[fingerprintString substringFromIndex:index] forKey:ELEMENT_FINGERPRINT_VALUE];
        }
    }
    
    return result;
}

@end
