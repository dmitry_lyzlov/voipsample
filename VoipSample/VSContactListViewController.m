//
//  VSContactListViewController.m
//  VoipSample
//
//  Created by Dmitry Lyzlov on 21/02/16.
//  Copyright © 2016 Dmitry Lyzlov. All rights reserved.
//

#import "VSContactListViewController.h"

#import "VSContactListCell.h"

@interface VSContactListViewController () < VSContactListCellDelegate >

@end

@implementation VSContactListViewController

#pragma mark - UITableViewDataSource

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString * const cellIdentifier = @"VSContactListCell";

    VSContactListCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    cell.actionDelegate = self;

    return cell;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    NSInteger number = 0;
    if ([self.contactListDataSource respondsToSelector:@selector(numberOfCotacts)])
    {
        number = [self.contactListDataSource numberOfCotacts];
    }
    return number;
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ([self.contactListDataSource respondsToSelector:@selector(contactForIndex:)])
    {
        VSContactListCell *contactCell = (VSContactListCell *)cell;
        contactCell.contactInfo = [self.contactListDataSource contactForIndex:indexPath.row];
    }
}

#pragma mark - UITableViewDelegate

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 60.0;
}

#pragma mark - VSContactListCellDelegate

- (void)cell:(VSContactListCell *)cell didSelectedAction:(VSContactListAction)action
{
    NSIndexPath *path = [self.tableView indexPathForCell:cell];
    if (path && [self.contactListDataSource respondsToSelector:@selector(selectedAction:forContactWithIndex:)])
    {
        [self.contactListDataSource selectedAction:action forContactWithIndex:path.row];
    }
}

#pragma mark - VSContactListDelegate

- (void)managerDidReceivedContacts:(id)manager
{
    [self.tableView reloadData];
}


@end
