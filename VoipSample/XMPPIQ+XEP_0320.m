//
//  XMPPIQ+XEP_0320.m
//  VoipSample
//
//  Created by Dmitry Lyzlov on 05/03/16.
//  Copyright © 2016 Dmitry Lyzlov. All rights reserved.
//

#import "XMPPIQ+XEP_0320.h"

#import <NSXMLElement+XMPP.h>

#import "XMPPIQ+Utilities.h"

@implementation XMPPIQ (XEP_0320)

- (NSXMLElement *)formatFingerprintElementWithData:(NSDictionary *)data
{

    /*
     <fingerprint xmlns='urn:xmpp:jingle:apps:dtls:0' hash='sha-256' setup='actpass'>
         02:1A:CC:54:27:AB:EB:9C:53:3F:3E:4B:65:2E:7D:46:3F:54:42:CD:54:F1:7A:03:A2:7D:F9:B0:7F:46:19:B2
     </fingerprint>
     */

    NSXMLElement *fingerprint = [NSXMLElement elementWithName:ELEMENT_FINGERPRINT URI:XMLNS_JINGLE_DTLS];
    [fingerprint setStringValue:data[ELEMENT_FINGERPRINT_VALUE]];
    [self fillElement:fingerprint withValuesFromDictionary:data byKeys:@[ ATTRIBUTE_HASH, ATTRIBUTE_SETUP ]];

    return fingerprint;
}

@end
