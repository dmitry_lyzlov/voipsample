//
//  VSVideoCallView.h
//  VoipSample
//
//  Created by Dmitry Lyzlov on 08/03/16.
//  Copyright © 2016 Dmitry Lyzlov. All rights reserved.
//

#import <UIKit/UIKit.h>

@class RTCVideoTrack;

@interface VSVideoCallView : UIView

- (void)setRemoteVideoTrack:(RTCVideoTrack *)remoteVideoTrack;
//- (void)setLocalVideoTrack:(RTCVideoTrack *)remoteVideoTrack;

@end
