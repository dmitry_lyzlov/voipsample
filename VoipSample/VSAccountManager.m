//
//  VSAccountManager.m
//  VoipSample
//
//  Created by Dmitry Lyzlov on 27/02/16.
//  Copyright © 2016 Dmitry Lyzlov. All rights reserved.
//

#import "VSAccountManager.h"

#import <GoogleSignIn.h>

#import "VSSignalingManager.h"
#import "VSContact.h"

static const int ddLogLevel = LOG_LEVEL_VERBOSE;

@interface VSAccountManager() < GIDSignInDelegate, GIDSignInUIDelegate, VSSignalingManagerDelegate >

@property (strong, nonatomic) VSSignalingManager *signalingManager;
@property (strong, nonatomic) NSMutableArray *contactList;

@end

@implementation VSAccountManager

#pragma mark - Properties

- (GIDProfileData *)currentUserProfile
{
    return [GIDSignIn sharedInstance].currentUser.profile;
}

- (VSSignalingManager *)signalingManager
{
    if (!_signalingManager)
    {
        _signalingManager = [[VSSignalingManager alloc] init];
        _signalingManager.delegate = self;
        self.contactList = [NSMutableArray array];
    }

    return _signalingManager;
}

#pragma mark - Public

+ (instancetype)sharedInstance
{
    static VSAccountManager *instance;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^
    {
        instance = [[VSAccountManager alloc] init];
    });

    return instance;
}

- (instancetype)init
{
    self = [super init];
    if (self)
    {
        GIDSignIn *signInInstance = [GIDSignIn sharedInstance];
        signInInstance.uiDelegate = self;
        signInInstance.delegate = self;
        signInInstance.scopes = @[ @"https://www.googleapis.com/auth/plus.login",
                                   @"https://www.googleapis.com/auth/googletalk" ];
    }

    return self;
}

- (void)signInSilently
{
    [[GIDSignIn sharedInstance] signInSilently];
}

- (void)signOut
{
    [[GIDSignIn sharedInstance] signOut];
}

- (void)connectToMessagingService
{
    GIDGoogleUser *currentUser = [GIDSignIn sharedInstance].currentUser;
    GIDAuthentication *auth = currentUser.authentication;
    self.signalingManager.accessToken = auth.accessToken;

    [self.signalingManager connectWithUserString:currentUser.profile.email];
}

- (void)disconnectFromMessagingService
{
    [self.signalingManager disconnect];
}

- (void)acceptCall
{
    [self.signalingManager acceptCall];
}

- (void)rejectCall
{
    [self.signalingManager terminateCall];
}

#pragma mark - VSContactListDataSource

- (NSUInteger)numberOfCotacts
{
    return self.contactList.count;
}

- (VSContact *)contactForIndex:(NSUInteger)index
{
    if (index >= [self numberOfCotacts])
    {
        return nil;
    }

    return [self.contactList objectAtIndex:index];
}

- (void)selectedAction:(VSContactListAction)action forContactWithIndex:(NSUInteger)index
{
    VSContact *contact = [self contactForIndex:index];
    [self.signalingManager initiateCallWithContact:contact useVideo:action == VSContactListAction_CallVideo];
}

#pragma mark - GIDSignInDelegate

- (void)signIn:(GIDSignIn *)signIn didSignInForUser:(GIDGoogleUser *)user withError:(NSError *)error
{
    if (error)
    {
        DDLogError(@"error code %ld: %@", (long)error.code, error.localizedDescription);
    }

    if ([self.signInDelegate respondsToSelector:@selector(accountManager:didSignedWithError:)])
    {
        dispatch_async(dispatch_get_main_queue(), ^
        {
            [self.signInDelegate accountManager:self didSignedWithError:error];
        });
    }
}

#pragma mark - GIDSignInUIDelegate

- (void)signInWillDispatch:(GIDSignIn *)signIn error:(NSError *)error
{
    if ([self.signInDelegate respondsToSelector:@selector(accountManagerWillSignIn:)])
    {
        dispatch_async(dispatch_get_main_queue(), ^
        {
            [self.signInDelegate accountManagerWillSignIn:self];
        });
    }
}

- (void)signIn:(GIDSignIn *)signIn presentViewController:(UIViewController *)viewController
{
    if ([self.signInDelegate respondsToSelector:@selector(accountManager:requestsPresentViewController:)])
    {
        dispatch_async(dispatch_get_main_queue(), ^
        {
            [self.signInDelegate accountManager:self requestsPresentViewController:viewController];
        });
    }
}

- (void)signIn:(GIDSignIn *)signIn dismissViewController:(UIViewController *)viewController
{
    if ([self.signInDelegate respondsToSelector:@selector(accountManager:requestsDismissViewController:)])
    {
        dispatch_async(dispatch_get_main_queue(), ^
        {
            [self.signInDelegate accountManager:self requestsDismissViewController:viewController];
        });
    }
}

#pragma mark - VSSignalingManagerDelegate

- (void)signalingManager:(VSSignalingManager *)manager didConnectWithError:(NSError *)error
{
    DDLogInfo(@"%s", __PRETTY_FUNCTION__);
}

- (void)signalingManager:(VSSignalingManager *)manager didDisconnectWithError:(NSError *)error
{
    DDLogInfo(@"%s", __PRETTY_FUNCTION__);

    self.signalingManager = nil;
}

- (void)signalingManager:(VSSignalingManager *)manager didReceivePresenceFromJID:(XMPPJID *)jid isOnline:(BOOL)online
{
    DDLogInfo(@"%s", __PRETTY_FUNCTION__);

    BOOL hasUpdate = NO;
    if (online)
    {
        hasUpdate = [self addContactWithJID:jid];
    }
    else
    {
        hasUpdate = [self removeContactWithJID:jid];
    }

    if (hasUpdate && [self.contactListDelegate respondsToSelector:@selector(managerDidReceivedContacts:)])
    {
        dispatch_async(dispatch_get_main_queue(), ^
        {
           [self.contactListDelegate managerDidReceivedContacts:self];
        });
    }
}

- (void)signalingManager:(VSSignalingManager *)manager callStateChanged:(VSCall *)call
{
    DDLogInfo(@"%s", __PRETTY_FUNCTION__);

    [[NSNotificationCenter defaultCenter] postNotificationName:VS_NOTIFICATION_CALL_STATE_CHANGED object:call];

    if ([self.callDelegate respondsToSelector:@selector(accountManager:callStateChanged:)])
    {
        [self.callDelegate accountManager:self callStateChanged:call];
    }
}

- (void)signalingManager:(VSSignalingManager *)manager didReceiveRemoteVideoTrack:(RTCVideoTrack *)videoTrack
{
    DDLogInfo(@"%s", __PRETTY_FUNCTION__);

    if ([self.callDelegate respondsToSelector:@selector(accountManager:didReceiveRemoteVideoTrack:)])
    {
        [self.callDelegate accountManager:self didReceiveRemoteVideoTrack:videoTrack];
    }
}

- (void)signalingManager:(VSSignalingManager *)manager didReceiveLocalVideoTrack:(RTCVideoTrack *)videoTrack
{
    DDLogInfo(@"%s", __PRETTY_FUNCTION__);

    if ([self.callDelegate respondsToSelector:@selector(accountManager:didReceiveLocalVideoTrack:)])
    {
        [self.callDelegate accountManager:self didReceiveLocalVideoTrack:videoTrack];
    }
}

#pragma mark - Private

- (BOOL)addContactWithJID:(XMPPJID *)jid
{
    DDLogInfo(@"%s", __PRETTY_FUNCTION__);

    BOOL result = NO;
    if ([self indexOfContactWithJID:jid] == NSNotFound)
    {
        result = YES;
        [self.contactList addObject:[[VSContact alloc] initWithJID:jid]];
    }
    return result;
}

- (BOOL)removeContactWithJID:(XMPPJID *)jid
{
    DDLogInfo(@"%s", __PRETTY_FUNCTION__);

    BOOL result = NO;
    NSUInteger index = [self indexOfContactWithJID:jid];
    if (index != NSNotFound)
    {
        result = YES;
        [self.contactList removeObjectAtIndex:index];
    }
    return result;
}

- (NSUInteger)indexOfContactWithJID:(XMPPJID *)jid
{
    return [self.contactList indexOfObjectPassingTest:^BOOL(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop)
    {
        BOOL match = [(VSContact *)obj isEqualToJID:jid];
        if (match)
        {
            *stop = YES;
        }
        return match;
    }];
}

@end
