//
//  XMPPJingle.h
//  VoipSample
//
//  Created by Dmitry Lyzlov on 28/02/16.
//  Copyright © 2016 Dmitry Lyzlov. All rights reserved.
//

#import <XMPP.h>

@class XMPPJingle;
@class XMPPIQ;

@protocol XMPPJingleDelegate < NSObject >

@optional
- (void)jingle:(XMPPJingle *)jingle didCreatedSessionIQ:(XMPPIQ *)iq;
- (void)jingle:(XMPPJingle *)jingle didReceivedSessionInitiateIQ:(XMPPIQ *)iq;
- (void)jingle:(XMPPJingle *)jingle didReceivedSessionAcceptIQ:(XMPPIQ *)iq;
- (void)jingle:(XMPPJingle *)jingle didReceivedSessionTerminateIQ:(XMPPIQ *)iq;
- (void)jingle:(XMPPJingle *)jingle didReceivedRingingIQ:(XMPPIQ *)iq;

@end

@interface XMPPJingle : XMPPModule

@property (weak, nonatomic) id< XMPPJingleDelegate > jingleDelegate;

- (NSString *)initiateSessionWithJIDTo:(XMPPJID *)to
                withSessionDescription:(NSString *)sessionDescription
                   transportCandidates:(NSArray *)candidates;

- (void)acceptSessionWithSid:(NSString *)sid
                       JIDTo:(XMPPJID *)to
      withSessionDescription:(NSString *)sessionDescription
         transportCandidates:(NSArray *)candidates;

- (void)terminateSessionWithSid:(NSString *)sid
                             to:(XMPPJID *)to
                    isInitiator:(BOOL)isInitiator
                        success:(BOOL)success;

@end
