//
//  VSContactListDelegation.h
//  VoipSample
//
//  Created by Dmitry Lyzlov on 29/02/16.
//  Copyright © 2016 Dmitry Lyzlov. All rights reserved.
//

#ifndef VSContactListDelegation_h
#define VSContactListDelegation_h

typedef NS_ENUM(NSInteger, VSContactListAction)
{
    VSContactListAction_None = 0,
    VSContactListAction_CallAudio,
    VSContactListAction_CallVideo
};

@class VSContact;

@protocol VSContactListDataSource < NSObject >

- (NSUInteger)numberOfCotacts;
- (VSContact *)contactForIndex:(NSUInteger)index;
- (void)selectedAction:(VSContactListAction)action forContactWithIndex:(NSUInteger)index;

@end

@protocol VSContactListDelegate < NSObject >

- (void)managerDidReceivedContacts:(id)manager;
// TODO: make methods for single contact

@end

#endif /* VSContactListDelegation_h */
