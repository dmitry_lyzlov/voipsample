//
//  XMPPJingle.m
//  VoipSample
//
//  Created by Dmitry Lyzlov on 28/02/16.
//  Copyright © 2016 Dmitry Lyzlov. All rights reserved.
//

#import "XMPPJingle.h"

#import <XMPPLogging.h>

#import "XMPPIQ+Utilities.h"
#import "XMPPIQ+XEP_0167.h"
#import "XMPPIQ+XEP_0176.h"
#import "XMPPIQ+XEP_0320.h"

static const int ddLogLevel = LOG_LEVEL_VERBOSE;

NSString * const IQ_TYPE_SET = @"set";
NSString * const IQ_TYPE_RESULT = @"result";

@interface XMPPJingle() < XMPPStreamDelegate >

@property (strong, nonatomic) NSString *currentSessionId;

@end

@implementation XMPPJingle

#pragma mark - Public

- (NSString *)initiateSessionWithJIDTo:(XMPPJID *)to withSessionDescription:(NSString *)sessionDescription transportCandidates:(NSArray *)candidates
{
    DDLogInfo(@"%s", __PRETTY_FUNCTION__);

    XMPPStream *stream = self.xmppStream;
    XMPPIQ *message = [self generateSessionMessageWithAction:JingleAction_SessionInitiate
                                                          to:to
                                                   sessionId:[stream generateUUID]
                                      withSessionDescription:sessionDescription
                                         transportCandidates:candidates
                                                 isInitiator:YES];
    self.currentSessionId = [message getSid];
    [stream sendElement:message];

    return self.currentSessionId;
}

- (void)acceptSessionWithSid:(NSString *)sid JIDTo:(XMPPJID *)to withSessionDescription:(NSString *)sessionDescription transportCandidates:(NSArray *)candidates
{
    XMPPStream *stream = self.xmppStream;
    XMPPIQ *message = [self generateSessionMessageWithAction:JingleAction_SessionAccept
                                                          to:to
                                                   sessionId:sid
                                      withSessionDescription:sessionDescription
                                         transportCandidates:candidates
                                                 isInitiator:NO];
    [stream sendElement:message];
}

- (void)terminateSessionWithSid:(NSString *)sid to:(XMPPJID *)to isInitiator:(BOOL)isInitiator success:(BOOL)success
{
    DDLogInfo(@"%s", __PRETTY_FUNCTION__);

    XMPPStream *stream = self.xmppStream;
    XMPPIQ *message = [self generateSessionTerminateMessageWithTo:to sessionId:sid isInitiator:isInitiator success:success];
    [stream sendElement:message];
}

#pragma mark - XMPPStreamDelegate

- (void)xmppStream:(XMPPStream *)sender didSendIQ:(XMPPIQ *)iq
{
    if (![iq isJingleMessage])
    {
        return;
    }

    DDLogInfo(@"%s", __PRETTY_FUNCTION__);

    dispatch_block_t block = ^
    {
        @autoreleasepool
        {
            if ([iq isSetIQ] && [[iq getSid] isEqualToString:self.currentSessionId])
            {
                [(id< XMPPJingleDelegate >)multicastDelegate jingle:self didCreatedSessionIQ:iq];
            }
        }
    };

    if (dispatch_get_specific(moduleQueueTag))
    {
        block();
    }
    else
    {
        dispatch_async(moduleQueue, block);
    }
}

- (BOOL)xmppStream:(XMPPStream *)sender didReceiveIQ:(XMPPIQ *)iq
{
    if (![iq isJingleMessage])
    {
        return NO;
    }

    DDLogInfo(@"%s", __PRETTY_FUNCTION__);

    if ([iq isSetIQ])
    {
        JingleAction action = [iq getAction];
        switch (action)
        {
            case JingleAction_SessionInitiate:
                [self onSessionInitiateIQ:iq];
                break;
            case JingleAction_SessionInfo:
                [self onSessionInfoIQ:iq];
                break;
            case JingleAction_SessionAccept:
                [self onSessionAccept:iq];
                break;
            case JingleAction_SessionTerminate:
                [self onSessionTerminate:iq];
                break;
            default:
                break;
        }
    }
    else if ([iq isResultIQ])
    {

    }
    else if ([iq isErrorIQ])
    {
        DDLogError(@"%s - Error iq: %@", __PRETTY_FUNCTION__, iq);
    }

    return YES;
}

#pragma mark - Private

- (void)onSessionInitiateIQ:(XMPPIQ *)iq
{
    DDLogInfo(@"%s - %@", __PRETTY_FUNCTION__, iq);

    dispatch_block_t block = ^
    {
        @autoreleasepool
        {
            XMPPStream *stream = self.xmppStream;
            [stream sendElement:[self generateACKForMessage:iq]];
            [stream sendElement:[self generateRingingForMessage:iq]];

            [(id< XMPPJingleDelegate >)multicastDelegate jingle:self didReceivedSessionInitiateIQ:iq];
        }
    };

    if (dispatch_get_specific(moduleQueueTag))
    {
        block();
    }
    else
    {
        dispatch_async(moduleQueue, block);
    }
}

- (void)onSessionInfoIQ:(XMPPIQ *)iq
{
    DDLogInfo(@"%s - %@", __PRETTY_FUNCTION__, iq);

    dispatch_block_t block = ^
    {
        @autoreleasepool
        {
            [self.xmppStream sendElement:[self generateACKForMessage:iq]];

            [(id< XMPPJingleDelegate >)multicastDelegate jingle:self didReceivedRingingIQ:iq];
        }
    };

    [self dispatchBlock:block];
}

- (void)onSessionAccept:(XMPPIQ *)iq
{
    DDLogInfo(@"%s - %@", __PRETTY_FUNCTION__, iq);

    dispatch_block_t block = ^
    {
        @autoreleasepool
        {
            [self.xmppStream sendElement:[self generateACKForMessage:iq]];

            [(id< XMPPJingleDelegate >)multicastDelegate jingle:self didReceivedSessionAcceptIQ:iq];
        }
    };

    [self dispatchBlock:block];
}

- (void)onSessionTerminate:(XMPPIQ *)iq
{
    DDLogInfo(@"%s - %@", __PRETTY_FUNCTION__, iq);

    dispatch_block_t block = ^
    {
        @autoreleasepool
        {
            [self.xmppStream sendElement:[self generateACKForMessage:iq]];

            [(id< XMPPJingleDelegate >)multicastDelegate jingle:self didReceivedSessionTerminateIQ:iq];
        }
    };

    [self dispatchBlock:block];
}

- (void)dispatchBlock:(dispatch_block_t)block
{
    if (dispatch_get_specific(moduleQueueTag))
    {
        block();
    }
    else
    {
        dispatch_async(moduleQueue, block);
    }
}

#pragma mark - Private (Generate messages)

- (XMPPIQ *)generateSessionMessageWithAction:(JingleAction)action
                                          to:(XMPPJID *)to
                                   sessionId:(NSString *)sessionId
                      withSessionDescription:(NSString *)sessionDescription
                         transportCandidates:(NSArray *)candidates
                                 isInitiator:(BOOL)isInitiator
{
    /*
     <iq from='romeo@montague.lit/orchard'
         id='ih28sx61'
         to='juliet@capulet.lit/balcony'
         type='set'>
         <jingle xmlns='urn:xmpp:jingle:1'
             action='session-initiate'
             initiator='romeo@montague.lit/orchard'
             sid='a73sjjvkla37jfea'>
             <content creator='initiator' name='voice'>
                 <description xmlns='urn:xmpp:jingle:apps:rtp:1' media='audio'>
                     <payload-type id='96' name='speex' clockrate='16000'/>
                     <payload-type id='97' name='speex' clockrate='8000'/>
                     <payload-type id='18' name='G729'/>
                     <payload-type id='0' name='PCMU'/>
                     <payload-type id='103' name='L16' clockrate='16000' channels='2'/>
                     <payload-type id='98' name='x-ISAC' clockrate='8000'/>
                 </description>
                 <transport xmlns='urn:xmpp:jingle:transports:ice-udp:1'
                     pwd='asd88fgpdd777uzjYhagZg'
                     ufrag='8hhy'>
                     <candidate component='1'
                         foundation='1'
                         generation='0'
                         id='el0747fg11'
                         ip='10.0.1.1'
                         network='1'
                         port='8998'
                         priority='2130706431'
                         protocol='udp'
                         type='host'/>
                     <candidate component='1'
                         foundation='2'
                         generation='0'
                         id='y3s2b30v3r'
                         ip='192.0.2.3'
                         network='1'
                         port='45664'
                         priority='1694498815'
                         protocol='udp'
                         rel-addr='10.0.1.1'
                         rel-port='8998'
                         type='srflx'/>
                 </transport>
             </content>
         </jingle>
     </iq>
     */

    XMPPStream *stream = self.xmppStream;
    XMPPIQ *message = [XMPPIQ iqWithType:IQ_TYPE_SET to:to elementID:[stream generateUUID]];

    NSXMLElement *jingle = [self generateJingleElementWithMessageIQ:message action:action to:to sessionId:sessionId isInitiator:isInitiator];

    NSMutableString *sessionDescriptionCopy = [sessionDescription mutableCopy];
    NSDictionary *sessionDescriptionData = [[XMPPIQ class] extractJingleRTPDataFromSDPString:sessionDescriptionCopy];

    NSXMLElement *content = [message formatContentElement];
    NSXMLElement *description = [message formatDescriptionElementWithSDPString:sessionDescriptionCopy];
    NSXMLElement *transport = [message formatTransportElementWithICEUDPData:sessionDescriptionData candidates:candidates];
    NSXMLElement *fingerprint = [message formatFingerprintElementWithData:sessionDescriptionData];

    [transport addChild:fingerprint];
    [content addChild:description];
    [content addChild:transport];
    [jingle addChild:content];

    return message;
}

- (XMPPIQ *)generateSessionTerminateMessageWithTo:(XMPPJID *)to
                                        sessionId:(NSString *)sessionId
                                      isInitiator:(BOOL)isInitiator
                                          success:(BOOL)success
{
    /*
     <iq from='romeo@montague.lit/orchard'
         id='fl2v387j'
         to='juliet@capulet.lit/balcony'
         type='set'>
         <jingle xmlns='urn:xmpp:jingle:1'
             action='session-terminate'
             initiator='romeo@montague.lit/orchard'
             sid='a73sjjvkla37jfea'>
             <reason>
                 <success/>
                 <text>I&apos;m outta here!</text>
             </reason>
         </jingle>
     </iq>
     */

    XMPPStream *stream = self.xmppStream;
    XMPPIQ *message = [XMPPIQ iqWithType:IQ_TYPE_SET to:to elementID:[stream generateUUID]];

    NSXMLElement *jingle = [self generateJingleElementWithMessageIQ:message action:JingleAction_SessionTerminate to:to sessionId:sessionId isInitiator:isInitiator];
    NSXMLElement *reason = [message formatReasonElementWithSuccess:success];

    [jingle addChild:reason];

    return message;
}

- (NSXMLElement *)generateJingleElementWithMessageIQ:(XMPPIQ *)message
                                              action:(JingleAction)action
                                                  to:(XMPPJID *)to
                                           sessionId:(NSString *)sessionId
                                         isInitiator:(BOOL)isInitiator
{
    /*
     <iq from='romeo@montague.lit/orchard'
         id='ih28sx61'
         to='juliet@capulet.lit/balcony'
         type='set'>
         <jingle xmlns='urn:xmpp:jingle:1'
             action='session-initiate'
             initiator='romeo@montague.lit/orchard'
             sid='a73sjjvkla37jfea'>
             ...
         </jingle>
     </iq>
     */

    XMPPStream *stream = self.xmppStream;
    XMPPJID *from = stream.myJID;
    XMPPJID *initiator = isInitiator ? from : to;
    XMPPJID *responder = isInitiator ? nil : from;

    [message addAttributeWithName:ATTRIBUTE_FROM stringValue:from.full];

    NSXMLElement *jingle = [message formatJinleElementWithAction:action
                                                       initiator:initiator
                                                       responder:responder
                                                       sessionId:sessionId];
    [message addChild:jingle];

    return jingle;
}

- (XMPPIQ *)generateACKForMessage:(XMPPIQ *)receivedMessage
{
    /*
     <iq from='juliet@capulet.lit/balcony'
         id='ih28sx61'
         to='romeo@montague.lit/orchard'
         type='result'/>
     */

    XMPPStream *stream = self.xmppStream;
    XMPPIQ *message = [XMPPIQ iqWithType:IQ_TYPE_RESULT
                                      to:receivedMessage.from
                               elementID:receivedMessage.elementID];
    [message addAttributeWithName:ATTRIBUTE_FROM stringValue:stream.myJID.full];

    return message;
}

- (XMPPIQ *)generateRingingForMessage:(XMPPIQ *)receivedMessage
{
    /*
     <iq from='juliet@capulet.lit/balcony'
         id='hq7rg186'
         to='romeo@montague.lit/orchard'
         type='set'>
         <jingle xmlns='urn:xmpp:jingle:1'
             action='session-info'
             sid='a73sjjvkla37jfea'>
             <ringing xmlns='urn:xmpp:jingle:apps:rtp:1:info'/>
         </jingle>
     </iq>
     */

    XMPPStream *stream = self.xmppStream;
    XMPPIQ *message = [XMPPIQ iqWithType:IQ_TYPE_SET
                                      to:receivedMessage.from
                               elementID:[stream generateUUID]];
    [message addAttributeWithName:ATTRIBUTE_FROM stringValue:stream.myJID.full];

    NSXMLElement *jingle = [message formatJinleElementWithAction:JingleAction_SessionInfo
                                                       initiator:nil
                                                       responder:nil
                                                       sessionId:[receivedMessage getSid]];
    NSXMLElement *ringing = [message formatRingingElement];

    [jingle addChild:ringing];
    [message addChild:jingle];

    return message;
}

@end
