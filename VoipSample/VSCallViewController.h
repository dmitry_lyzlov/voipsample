//
//  VSCallViewController.h
//  VoipSample
//
//  Created by Dmitry Lyzlov on 02/03/16.
//  Copyright © 2016 Dmitry Lyzlov. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface VSCallViewController : UIViewController

@property (assign, nonatomic) BOOL acceptButtonAvailable;

@end
