//
//  VSContactListViewController.h
//  VoipSample
//
//  Created by Dmitry Lyzlov on 21/02/16.
//  Copyright © 2016 Dmitry Lyzlov. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "VSContactListDelegation.h"

@interface VSContactListViewController : UITableViewController < VSContactListDelegate >

@property (weak, nonatomic) id< VSContactListDataSource > contactListDataSource;

@end
