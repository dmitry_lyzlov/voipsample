//
//  VSMediaEngine.m
//  VoipSample
//
//  Created by Dmitry Lyzlov on 21/02/16.
//  Copyright © 2016 Dmitry Lyzlov. All rights reserved.
//

#import "VSMediaManager.h"

#import <RTCPeerConnectionFactory.h>
#import <RTCICEServer.h>
#import <RTCMediaConstraints.h>
#import <RTCPeerConnectionInterface.h>
#import <RTCPeerConnection.h>
#import <RTCMediaStream.h>
#import <RTCPair.h>
#import <RTCSessionDescription.h>
#import <RTCSessionDescriptionDelegate.h>
#import <RTCICECandidate.h>
#import <RTCVideoTrack.h>
#import <RTCAVFoundationVideoSource.h>

#import <DDLog.h>

#define SDP_M_VIDEO @"m=video"

static const int ddLogLevel = LOG_LEVEL_VERBOSE;

@interface VSMediaManager() < RTCPeerConnectionDelegate, RTCSessionDescriptionDelegate >

@property (weak, nonatomic) id< VSMediaManagerDelegate > delegate;

// webRTC
@property (strong, nonatomic, readonly) RTCPeerConnectionFactory *peerConnectionFactory;
@property (strong, nonatomic) RTCPeerConnection *peerConnection;

@property (strong, nonatomic) NSMutableArray< RTCICECandidate * > *iceCandidates;

@end

@implementation VSMediaManager

#pragma mark - Properties

- (RTCPeerConnectionFactory *)peerConnectionFactory
{
    static RTCPeerConnectionFactory *peerConnectionFactoryInstance;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^
    {
        peerConnectionFactoryInstance = [[RTCPeerConnectionFactory alloc] init];
    });

    return peerConnectionFactoryInstance;
}

- (NSString *)localSessionDescription
{
    return self.peerConnection.localDescription.description;
}

- (NSString *)remoteSessionDescription
{
    return self.peerConnection.remoteDescription.description;
}

- (void)setRemoteSessionDescription:(NSString *)remoteSessionDescription
{
    RTCSessionDescription *sessionDescription = [[RTCSessionDescription alloc] initWithType:@"answer" sdp:remoteSessionDescription];
    [self.peerConnection setRemoteDescriptionWithDelegate:self sessionDescription:sessionDescription];
}

- (NSArray *)iceCandidatesSDP
{
    return [self.iceCandidates valueForKeyPath:@"@unionOfObjects.sdp"];
}

#pragma mark - Public

- (instancetype)initWithDelegate:(id< VSMediaManagerDelegate >)delegate
{
    DDLogInfo(@"%s", __PRETTY_FUNCTION__);

    self = [super init];
    if (self)
    {
        self.delegate = delegate;
        self.iceCandidates = [NSMutableArray array];
    }

    return self;
}

- (void)openConnectionAsInitiatorWithVideo:(BOOL)useVideo
{
    [self openConnectionWithSessionDescription:nil candidates:nil useVideo:useVideo];
}

- (void)openConnectionAsResponderWithSessionDescription:(NSString *)sessionDescription candidates:(NSArray *)candidates
{
    BOOL useVideo = [sessionDescription containsString:SDP_M_VIDEO];
    [self openConnectionWithSessionDescription:sessionDescription candidates:candidates useVideo:useVideo];
}

- (void)sendAnswer
{
    RTCPeerConnection *peerConnection = self.peerConnection;
    if (peerConnection.signalingState == RTCSignalingHaveRemoteOffer && !peerConnection.localDescription)
    {
        BOOL useVideo = [self.remoteSessionDescription.description containsString:SDP_M_VIDEO];
        [peerConnection createAnswerWithDelegate:self
                                     constraints:[self defaultOfferAnswerConstraintsWithVideo:useVideo]];
    }
}

- (void)closeConnection
{
    DDLogInfo(@"%s", __PRETTY_FUNCTION__);

    [self.peerConnection close];
}

#pragma mark - RTCPeerConnectionDelegate

- (void)peerConnection:(RTCPeerConnection *)peerConnection signalingStateChanged:(RTCSignalingState)stateChanged
{
    DDLogInfo(@"%s - state %u", __PRETTY_FUNCTION__, stateChanged);

    dispatch_async(dispatch_get_main_queue(), ^
    {
        if (stateChanged == RTCSignalingClosed)
        {
            if ([self.delegate respondsToSelector:@selector(mediaManagerSignalingClosed:)])
            {
                [self.delegate mediaManagerSignalingClosed:self];
            }
        }
    });
}

- (void)peerConnection:(RTCPeerConnection *)peerConnection addedStream:(RTCMediaStream *)stream
{
    DDLogInfo(@"%s", __PRETTY_FUNCTION__);

    dispatch_async(dispatch_get_main_queue(), ^
    {
        if (stream.videoTracks.count > 0)
        {
            if ([self.delegate respondsToSelector:@selector(mediaManager:didReceivedRemoteVideoTrack:)])
            {
                [self.delegate mediaManager:self didReceivedRemoteVideoTrack:stream.videoTracks.firstObject];
            }
        }
    });
}

- (void)peerConnection:(RTCPeerConnection *)peerConnection removedStream:(RTCMediaStream *)stream
{
    DDLogInfo(@"%s", __PRETTY_FUNCTION__);
}

- (void)peerConnectionOnRenegotiationNeeded:(RTCPeerConnection *)peerConnection
{
    DDLogInfo(@"%s", __PRETTY_FUNCTION__);
}

- (void)peerConnection:(RTCPeerConnection *)peerConnection iceConnectionChanged:(RTCICEConnectionState)newState
{
    DDLogInfo(@"%s", __PRETTY_FUNCTION__);
}

- (void)peerConnection:(RTCPeerConnection *)peerConnection iceGatheringChanged:(RTCICEGatheringState)newState
{
    DDLogInfo(@"%s - state %u", __PRETTY_FUNCTION__, newState);

    dispatch_async(dispatch_get_main_queue(), ^
    {
        if (newState == RTCICEGatheringComplete)
        {
            if ([self.delegate respondsToSelector:@selector(mediaManagerICENegotioationCompleted:)])
            {
                [self.delegate mediaManagerICENegotioationCompleted:self];
            }
        }
    });
}

- (void)peerConnection:(RTCPeerConnection *)peerConnection gotICECandidate:(RTCICECandidate *)candidate
{
    DDLogInfo(@"%s - ICE Candidate: %@", __PRETTY_FUNCTION__, candidate);

    dispatch_async(dispatch_get_main_queue(), ^
    {
        [self.iceCandidates addObject:candidate];
    });
}

- (void)peerConnection:(RTCPeerConnection*)peerConnection didOpenDataChannel:(RTCDataChannel*)dataChannel
{
    DDLogInfo(@"%s", __PRETTY_FUNCTION__);
}

#pragma mark - RTCSessionDescriptionDelegate

- (void)peerConnection:(RTCPeerConnection *)peerConnection didCreateSessionDescription:(RTCSessionDescription *)sdp error:(NSError *)error
{
    DDLogInfo(@"%s", __PRETTY_FUNCTION__);

    dispatch_async(dispatch_get_main_queue(), ^
    {
        if (error)
        {
            DDLogError(@"%s - code %ld, %@", __PRETTY_FUNCTION__, (long)error.code, error.description);
            [self closeConnection];
            if ([self.delegate respondsToSelector:@selector(mediaManager:errorOccured:)])
            {
                [self.delegate mediaManager:self errorOccured:error];
            }
            return;
        }

        [peerConnection setLocalDescriptionWithDelegate:self
                                     sessionDescription:sdp];
    });
}

- (void)peerConnection:(RTCPeerConnection *)peerConnection didSetSessionDescriptionWithError:(NSError *)error
{
    DDLogInfo(@"%s", __PRETTY_FUNCTION__);

    dispatch_async(dispatch_get_main_queue(), ^
    {
        if (error)
        {
            DDLogError(@"%s - code %ld, %@", __PRETTY_FUNCTION__, (long)error.code, error.description);
            [self closeConnection];
            if ([self.delegate respondsToSelector:@selector(mediaManager:errorOccured:)])
            {
                [self.delegate mediaManager:self errorOccured:error];
            }
            return;
        }

        if ([self.delegate respondsToSelector:@selector(mediaManagerDidSetSessionDescription:)])
        {
            [self.delegate mediaManagerDidSetSessionDescription:self];
        }
    });
}

#pragma mark - Private

- (void)openConnectionWithSessionDescription:(NSString *)sessionDescription candidates:(NSArray *)candidates useVideo:(BOOL)useVideo
{
    DDLogInfo(@"%s", __PRETTY_FUNCTION__);

    NSArray *optionalConstraints = @[ [[RTCPair alloc] initWithKey:@"DtlsSrtpKeyAgreement" value:@"true"] ];
    RTCMediaConstraints *connectionConstraints = [[RTCMediaConstraints alloc] initWithMandatoryConstraints:nil
                                                                                       optionalConstraints:optionalConstraints];
    RTCPeerConnection *connection = [self.peerConnectionFactory peerConnectionWithICEServers:[self defaultSTUNServers]
                                                                                 constraints:connectionConstraints
                                                                                    delegate:self];

    RTCMediaStream *localStream = [self createLocalMediaStreamWithVideo:useVideo];
    [connection addStream:localStream];

    if (sessionDescription)
    {
//        for (NSString *item in candidates)
//        {
//            RTCICECandidate *candidate = [[RTCICECandidate alloc] initWithMid:@"audio" index:0 sdp:item]; // TODO: hardcoded Mid & m-line index
//            [connection addICECandidate:candidate];
//        }

        RTCSessionDescription *sessionDescriptionObj = [[RTCSessionDescription alloc] initWithType:@"offer" sdp:sessionDescription];
        [connection setRemoteDescriptionWithDelegate:self sessionDescription:sessionDescriptionObj];
    }
    else
    {
        [connection createOfferWithDelegate:self constraints:[self defaultOfferAnswerConstraintsWithVideo:useVideo]];
    }
    
    self.peerConnection = connection;
}

- (RTCMediaStream *)createLocalMediaStreamWithVideo:(BOOL)useVideo
{
    DDLogInfo(@"%s", __PRETTY_FUNCTION__);

    // TODO: currently audio only
    RTCPeerConnectionFactory *factory = self.peerConnectionFactory;
    RTCMediaStream *localStream = [factory mediaStreamWithLabel:@"VSMS"];
    [localStream addAudioTrack:[factory audioTrackWithID:@"VSMSa0"]];

#if !TARGET_IPHONE_SIMULATOR
    if (useVideo)
    {
        RTCMediaConstraints *videoConstraints = [[RTCMediaConstraints alloc] initWithMandatoryConstraints:nil
                                                                                      optionalConstraints:nil];
        RTCAVFoundationVideoSource *source = [[RTCAVFoundationVideoSource alloc] initWithFactory:factory
                                                                                     constraints:videoConstraints];
        RTCVideoTrack *localVideoTrack = [[RTCVideoTrack alloc] initWithFactory:factory
                                                                         source:source
                                                                        trackId:@"VSMSv0"];
        [localStream addVideoTrack:localVideoTrack];
        dispatch_async(dispatch_get_main_queue(), ^
        {
            if ([self.delegate respondsToSelector:@selector(mediaManager:didReceivedLocalVideoTrack:)])
            {
                [self.delegate mediaManager:self didReceivedLocalVideoTrack:localVideoTrack];
            }
        });
    }
#endif

    return localStream;
}

#pragma mark - Helpers

- (NSArray *)defaultSTUNServers
{
    static NSArray *defultSTUNSevers = nil;
    if (defultSTUNSevers)
    {
        return defultSTUNSevers;
    }

    NSString *serverString = @"stun.l.google.com:19302";

//    NSString *serverString =
//    @"stun.l.google.com:19302,\
//    stun1.l.google.com:19302,\
//    stun2.l.google.com:19302,\
//    stun3.l.google.com:19302,\
//    stun4.l.google.com:19302,\
//    stun.ekiga.net,\
//    stun.ideasip.com,\
//    stun.rixtelecom.se,\
//    stun.schlund.de,\
//    stun.stunprotocol.org:3478,\
//    stun.voiparound.com,\
//    stun.voipbuster.com,\
//    stun.voipstunt.com,\
//    stun.voxgratia.org";

    NSArray *serverStringArray = [serverString componentsSeparatedByString:@","];
    NSMutableArray *resultArray = [NSMutableArray arrayWithCapacity:serverStringArray.count];
    for (NSString *item in serverStringArray)
    {
        NSString *host = [item stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
        NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"stun:%@", host]];
        RTCICEServer *iceServer = [[RTCICEServer alloc] initWithURI:url username:@"" password:@""];
        if (iceServer)
        {
            [resultArray addObject:iceServer];
        }
    }
    defultSTUNSevers = resultArray;

    return defultSTUNSevers;
}

- (RTCMediaConstraints *)defaultOfferAnswerConstraintsWithVideo:(BOOL)useVideo
{
    NSString *offerVideoValue = useVideo ? @"true" : @"false";
    NSArray *mandatoryConstraints = @[ [[RTCPair alloc] initWithKey:@"OfferToReceiveAudio" value:@"true"],
                                       [[RTCPair alloc] initWithKey:@"OfferToReceiveVideo" value:offerVideoValue] ];
    return [[RTCMediaConstraints alloc] initWithMandatoryConstraints:mandatoryConstraints
                                                 optionalConstraints:nil];
}

@end
