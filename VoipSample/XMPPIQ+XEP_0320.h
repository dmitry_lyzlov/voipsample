//
//  XMPPIQ+XEP_0320.h
//  VoipSample
//
//  Created by Dmitry Lyzlov on 05/03/16.
//  Copyright © 2016 Dmitry Lyzlov. All rights reserved.
//

#import <XMPPIQ.h>

@interface XMPPIQ (XEP_0320)

- (NSXMLElement *)formatFingerprintElementWithData:(NSDictionary *)data;

@end
