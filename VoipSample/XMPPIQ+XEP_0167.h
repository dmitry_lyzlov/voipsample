//
//  XMPPIQ+XEP_0167.h
//  VoipSample
//
//  Created by Dmitry Lyzlov on 28/02/16.
//  Copyright © 2016 Dmitry Lyzlov. All rights reserved.
//

#import <XMPPIQ.h>

typedef NS_ENUM(NSInteger, JingleAction)
{
    JingleAction_Unknown = -1,
    JingleAction_SessionInitiate,
    JingleAction_SessionAccept,
    JingleAction_SessionTerminate,
    JingleAction_SessionInfo
};

typedef NS_ENUM(NSInteger, DescriptionMediaType)
{
    DescriptionMediaType_Audio,
    DescriptionMediaType_Video
};

@interface XMPPIQ (XEP_0167)

- (BOOL)isJingleMessage;

- (JingleAction)getAction;
- (NSString *)getSid;
- (NSString *)getSDPString;

- (NSXMLElement *)formatJinleElementWithAction:(JingleAction)action
                                     initiator:(XMPPJID *)initiator
                                     responder:(XMPPJID *)responder
                                     sessionId:(NSString *)sid;
- (NSXMLElement *)formatContentElement;
- (NSXMLElement *)formatDescriptionElementWithType:(DescriptionMediaType)type
                                       codecsArray:(NSArray *)codecs;
- (NSXMLElement *)formatDescriptionElementWithSDPString:(NSString *)SDPString;
- (NSXMLElement *)formatRingingElement;
- (NSXMLElement *)formatReasonElementWithSuccess:(BOOL)success;

@end
