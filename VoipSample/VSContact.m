//
//  VSContact.m
//  VoipSample
//
//  Created by Dmitry Lyzlov on 29/02/16.
//  Copyright © 2016 Dmitry Lyzlov. All rights reserved.
//

#import "VSContact.h"

#import <XMPPJID.h>

@interface VSContact()

@property (strong, nonatomic) XMPPJID *jid;

@end

@implementation VSContact

#pragma mark - Properties

- (NSString *)name
{
    return self.jid.user;
}

#pragma mark - Public

- (instancetype)initWithJID:(XMPPJID *)jid
{
    self = [super init];
    if (self)
    {
        self.jid = jid;
    }

    return self;
}

- (BOOL)isEqualToContact:(VSContact *)contact
{
    return [self isEqualToJID:contact.jid];
}

- (BOOL)isEqualToJID:(XMPPJID *)jid
{
    return [self.jid isEqualToJID:jid options:XMPPJIDCompareUser];
}

@end
