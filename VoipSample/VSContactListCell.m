//
//  VSContactTableViewCell.m
//  VoipSample
//
//  Created by Dmitry Lyzlov on 21/02/16.
//  Copyright © 2016 Dmitry Lyzlov. All rights reserved.
//

#import "VSContactListCell.h"

#import "VSCircleImageView.h"
#import "VSContact.h"

NSString * const OBSERVING_PATH_IMAGE = @"image";

@interface VSContactListCell()

@property (weak, nonatomic) IBOutlet VSCircleImageView *contactImage;
@property (weak, nonatomic) IBOutlet UILabel *contactName;
@property (weak, nonatomic) IBOutlet UIButton *audioCallButton;
@property (weak, nonatomic) IBOutlet UIButton *videoCallButton;

@end

@implementation VSContactListCell

#pragma mark - Properties

- (void)setContactInfo:(VSContact *)contactInfo
{
    if (_contactInfo != contactInfo)
    {
        _contactInfo = contactInfo;
        [self updateUI];
    }
}

#pragma mark - Public

- (void)awakeFromNib
{
    [self makeActionButtonsVisible:NO];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    [self makeActionButtonsVisible:selected];
}

- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary<NSString *,id> *)change context:(void *)context
{
    if (object != self.contactInfo)
    {
        return;
    }

    [self updateUI];
}

#pragma mark - Actions

- (IBAction)callButtonPressed:(UIButton *)sender
{
    if ([self.actionDelegate respondsToSelector:@selector(cell:didSelectedAction:)])
    {
        VSContactListAction action = VSContactListAction_None;
        if (sender == self.audioCallButton)
        {
            action = VSContactListAction_CallAudio;
        }
        else if (sender == self.videoCallButton)
        {
            action = VSContactListAction_CallVideo;
        }
        [self.actionDelegate cell:self didSelectedAction:action];
    }
}

#pragma mark - Private

- (void)makeActionButtonsVisible:(BOOL)visible
{
    self.audioCallButton.hidden = !visible;
    self.videoCallButton.hidden = !visible;
}

- (void)updateUI
{
    self.contactName.text = self.contactInfo.name;
    self.contactImage.url = [[self class] formatUrlFromString:self.contactInfo.image
                                       withSizeQueryParameter:(NSUInteger)self.contactImage.frame.size.width];
}

+ (NSURL *)formatUrlFromString:(NSString *)urlString withSizeQueryParameter:(NSUInteger)sizeParameter
{
    if (urlString.length == 0)
    {
        return nil;
    }

    static NSRegularExpression *regex = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^
    {
        regex = [NSRegularExpression regularExpressionWithPattern:@"\\\?.*sz=(\\d+)"
                                                          options:NSRegularExpressionCaseInsensitive
                                                            error:NULL];
    });

    NSRange queryRange = NSMakeRange(0, urlString.length);
    NSTextCheckingResult *result = [regex firstMatchInString:urlString
                                                     options:kNilOptions
                                                       range:queryRange];
    if (result.range.location != NSNotFound && result.numberOfRanges > 1)
    {
        queryRange = [result rangeAtIndex:1];
        urlString = [urlString stringByReplacingCharactersInRange:queryRange
                                                       withString:@(sizeParameter).stringValue];
    }

    return [NSURL URLWithString:urlString];
}

@end
