//
//  VSProfileViewController.m
//  VoipSample
//
//  Created by Dmitry Lyzlov on 28/02/16.
//  Copyright © 2016 Dmitry Lyzlov. All rights reserved.
//

#import "VSProfileViewController.h"

#import "VSAccountManager.h"

@implementation VSProfileViewController

#pragma mark - Actions

- (IBAction)logout
{
    [[VSAccountManager sharedInstance] disconnectFromMessagingService];
    [self.navigationController popToRootViewControllerAnimated:YES];
}

@end
