//
//  VSSignalingManager.h
//  VoipSample
//
//  Created by Dmitry Lyzlov on 21/02/16.
//  Copyright © 2016 Dmitry Lyzlov. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef NS_ENUM(NSInteger, VSSignalingManagerErrorCode)
{
    VSSignalingManagerError_Common = 1,
    VSSignalingManagerError_Timeout,
    VSSignalingManagerError_AuthError,
};

@class VSSignalingManager;
@class XMPPJID;
@class VSContact;
@class VSCall;
@class RTCVideoTrack;

@protocol VSSignalingManagerDelegate <NSObject>

- (void)signalingManager:(VSSignalingManager *)manager didConnectWithError:(NSError *)error;
- (void)signalingManager:(VSSignalingManager *)manager didDisconnectWithError:(NSError *)error;
- (void)signalingManager:(VSSignalingManager *)manager didReceivePresenceFromJID:(XMPPJID *)jid isOnline:(BOOL)online;

- (void)signalingManager:(VSSignalingManager *)manager callStateChanged:(VSCall *)call;
- (void)signalingManager:(VSSignalingManager *)manager didReceiveRemoteVideoTrack:(RTCVideoTrack *)videoTrack;
- (void)signalingManager:(VSSignalingManager *)manager didReceiveLocalVideoTrack:(RTCVideoTrack *)videoTrack;

@end

@interface VSSignalingManager : NSObject

@property (strong, nonatomic) NSString *accessToken;
@property (weak, nonatomic) id< VSSignalingManagerDelegate > delegate;

- (void)connectWithUserString:(NSString *)user;
- (void)disconnect;

- (void)initiateCallWithContact:(VSContact *)contact
                       useVideo:(BOOL)useVideo;
- (void)acceptCall;
- (void)terminateCall;

@end
