//
//  VSCall.h
//  VoipSample
//
//  Created by Dmitry Lyzlov on 03/03/16.
//  Copyright © 2016 Dmitry Lyzlov. All rights reserved.
//

#import <Foundation/Foundation.h>

@class VSContact;

typedef NS_ENUM(NSInteger, VSCallState)
{
    VSCallState_Unknown = -1,
    VSCallState_Created,
    VSCallState_Ringing,
    VSCallState_Rejected,
    VSCallState_Received,
    VSCallState_Connected,
    VSCallState_Terminated
};

@interface VSCall : NSObject

@property (strong, nonatomic, readonly) VSContact *contact;
@property (assign, nonatomic) VSCallState state;
@property (strong, nonatomic) NSString *sessionId;
@property (assign, nonatomic, readonly) BOOL isInitiator;

- (instancetype)initWithContact:(VSContact *)contact isInitiator:(BOOL)isInitiator;

@end
