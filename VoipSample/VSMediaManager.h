//
//  VSMediaEngine.h
//  VoipSample
//
//  Created by Dmitry Lyzlov on 21/02/16.
//  Copyright © 2016 Dmitry Lyzlov. All rights reserved.
//

#import <Foundation/Foundation.h>

@class VSMediaManager;
@class RTCVideoTrack;

@protocol VSMediaManagerDelegate < NSObject >

- (void)mediaManager:(VSMediaManager *)mediaManager errorOccured:(NSError *)error;
- (void)mediaManagerDidSetSessionDescription:(VSMediaManager *)mediaManager;
- (void)mediaManagerICENegotioationCompleted:(VSMediaManager *)mediaManager;
- (void)mediaManagerSignalingClosed:(VSMediaManager *)mediaManager;
- (void)mediaManager:(VSMediaManager *)mediaManager didReceivedRemoteVideoTrack:(RTCVideoTrack *)videoTrack;
- (void)mediaManager:(VSMediaManager *)mediaManager didReceivedLocalVideoTrack:(RTCVideoTrack *)videoTrack;

@end

@interface VSMediaManager : NSObject

@property (strong, nonatomic, readonly) NSString *localSessionDescription;
@property (strong, nonatomic) NSString *remoteSessionDescription;
@property (strong, nonatomic, readonly) NSArray< NSString * > *iceCandidatesSDP;

- (instancetype)initWithDelegate:(id< VSMediaManagerDelegate >)delegate;

- (void)openConnectionAsInitiatorWithVideo:(BOOL)useVideo;
- (void)openConnectionAsResponderWithSessionDescription:(NSString *)sessionDescription candidates:(NSArray *)candidates;
- (void)sendAnswer;

- (void)closeConnection;

@end
