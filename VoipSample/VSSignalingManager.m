//
//  VSSignalingManager.m
//  VoipSample
//
//  Created by Dmitry Lyzlov on 21/02/16.
//  Copyright © 2016 Dmitry Lyzlov. All rights reserved.
//

#import "VSSignalingManager.h"

#import <XMPP.h>
#import <XMPPLogging.h>
#import <XMPPXOAuth2Google.h>

#import "XMPPJingle.h"

#import "VSUtilities.h"
#import "VSMediaManager.h"
#import "VSCall.h"
#import "VSContact.h"
#import "XMPPIQ+XEP_0167.h"
#import "XMPPIQ+XEP_0176.h"

static const NSTimeInterval TIMEOUT = 5.0;
static const int ddLogLevel = LOG_LEVEL_VERBOSE;

NSString * const VSSIGNALINGMANAGER_ERROR_DOMAIN = @"VSSignalingManagerError";
NSString * const PRESENCE_UNAVAILABLE = @"unavailable";

@interface VSSignalingManager() < XMPPStreamDelegate, XMPPJingleDelegate, VSMediaManagerDelegate >

@property (strong, nonatomic) XMPPStream *stream;
@property (strong, nonatomic) XMPPXOAuth2Google *authentication;
@property (strong, nonatomic) XMPPJingle *jingle;

@property (strong, nonatomic) VSMediaManager *mediaManager;
@property (strong, nonatomic) VSCall *currentCall;

@end

@implementation VSSignalingManager

#pragma mark - Properties

- (VSMediaManager *)mediaManager
{
    if (!_mediaManager)
    {
        _mediaManager = [[VSMediaManager alloc] initWithDelegate:self];
    }

    return _mediaManager;
}

#pragma mark - Public

+ (instancetype)sharedInstance
{
    static VSSignalingManager *instance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^
    {
        instance = [[VSSignalingManager alloc] init];
    });

    return instance;
}

- (instancetype)init
{
    DDLogInfo(@"%s", __PRETTY_FUNCTION__);
    
    self = [super init];
    if (self)
    {
        dispatch_queue_t queue = dispatch_get_main_queue();

        XMPPStream *stream = [[XMPPStream alloc] init];
        [stream addDelegate:self delegateQueue:queue];
        self.stream = stream;

        XMPPJingle *jingle = [[XMPPJingle alloc] init];
        [jingle addDelegate:self delegateQueue:queue];
        [jingle activate:stream];
        [stream registerModule:jingle];
        self.jingle = jingle;
    }

    return self;
}

- (void)dealloc
{
    DDLogInfo(@"%s", __PRETTY_FUNCTION__);

    [self.jingle deactivate];
    [self.stream unregisterModule:self.jingle];
    [self.stream removeDelegate:self];
}

- (void)connectWithUserString:(NSString *)user
{
    DDLogInfo(@"%s", __PRETTY_FUNCTION__);

    self.stream.myJID = [XMPPJID jidWithString:user];
    NSError *error = nil;
    if (![self.stream connectWithTimeout:TIMEOUT error:&error])
    {
        DDLogError(@"%ld - %@", (long)error.code, error.description);

        if ([self.delegate respondsToSelector:@selector(signalingManager:didConnectWithError:)])
        {
            dispatch_async(dispatch_get_main_queue(), ^
            {
                [self.delegate signalingManager:self didConnectWithError:error];
            });
        }
    }
}

- (void)disconnect
{
    DDLogInfo(@"%s", __PRETTY_FUNCTION__);

    [self sendPresense:NO];
    [self.stream disconnectAfterSending];
}

- (void)initiateCallWithContact:(VSContact *)contact
                      useVideo:(BOOL)useVideo
{
    DDLogInfo(@"%s", __PRETTY_FUNCTION__);

    VSCall *call = [[VSCall alloc] initWithContact:contact isInitiator:YES];
    call.state = VSCallState_Created;
    self.currentCall = call;

    [self.mediaManager openConnectionAsInitiatorWithVideo:useVideo];

    [self notifyCallStateChanged];
}

- (void)acceptCall
{
    self.currentCall.state = VSCallState_Connected;

    [self.mediaManager sendAnswer];

    [self notifyCallStateChanged];
}

- (void)terminateCall
{
    DDLogInfo(@"%s", __PRETTY_FUNCTION__);

    [self.mediaManager closeConnection];

    self.currentCall.state = VSCallState_Terminated;
    [self sendSesionMessage];
    [self notifyCallStateChanged];
}

#pragma mark - XMPPJingleDelegate

- (void)jingle:(XMPPJingle *)jingle didCreatedSessionIQ:(XMPPIQ *)iq
{
    DDLogInfo(@"%s", __PRETTY_FUNCTION__);
}

- (void)jingle:(XMPPJingle *)jingle didReceivedSessionInitiateIQ:(XMPPIQ *)iq
{
    DDLogInfo(@"%s", __PRETTY_FUNCTION__);

    VSCall *call = [[VSCall alloc] initWithContact:[[VSContact alloc] initWithJID:iq.from] isInitiator:NO];
    call.state = VSCallState_Received;
    call.sessionId = [iq getSid];
    self.currentCall = call;

    [self.mediaManager openConnectionAsResponderWithSessionDescription:[iq getSDPString] candidates:[iq getCandidates]];

    [self notifyCallStateChanged];
}


- (void)jingle:(XMPPJingle *)jingle didReceivedSessionAcceptIQ:(XMPPIQ *)iq
{
    DDLogInfo(@"%s", __PRETTY_FUNCTION__);

    [self.mediaManager setRemoteSessionDescription:[iq getSDPString]];

    self.currentCall.state = VSCallState_Connected;
    [self notifyCallStateChanged];
}

- (void)jingle:(XMPPJingle *)jingle didReceivedSessionTerminateIQ:(XMPPIQ *)iq
{
    DDLogInfo(@"%s", __PRETTY_FUNCTION__);

    [self.mediaManager closeConnection];
    VSCall *call = self.currentCall;
    call.state = call.isInitiator && call.state != VSCallState_Connected ? VSCallState_Rejected : VSCallState_Terminated;
    [self notifyCallStateChanged];
}

- (void)jingle:(XMPPJingle *)jingle didReceivedRingingIQ:(XMPPIQ *)iq
{
    DDLogInfo(@"%s", __PRETTY_FUNCTION__);

    self.currentCall.state = VSCallState_Ringing;
    [self notifyCallStateChanged];
}

#pragma mark - XMPPStreamDelegate

- (void)xmppStreamDidConnect:(XMPPStream *)sender
{
    DDLogInfo(@"%s", __PRETTY_FUNCTION__);

    NSError *error = nil;
    self.authentication = [[XMPPXOAuth2Google alloc] initWithStream:self.stream
                                                        accessToken:self.accessToken];
    if (![self.stream authenticate:self.authentication error:&error])
    {
        DDLogError(@"%ld - %@", (long)error.code, error.description);
    }
}

- (void)xmppStreamConnectDidTimeout:(XMPPStream *)sender
{
    DDLogInfo(@"%s", __PRETTY_FUNCTION__);

    if ([self.delegate respondsToSelector:@selector(signalingManager:didConnectWithError:)])
    {
        NSError *error = [NSError errorWithDomain:VSSIGNALINGMANAGER_ERROR_DOMAIN
                                             code:VSSignalingManagerError_Timeout
                                         userInfo:nil];
        dispatch_async(dispatch_get_main_queue(), ^
        {
            [self.delegate signalingManager:self didConnectWithError:error];
        });
    }
}

- (void)xmppStreamDidDisconnect:(XMPPStream *)sender withError:(NSError *)error
{
    DDLogInfo(@"%s", __PRETTY_FUNCTION__);

    if ([self.delegate respondsToSelector:@selector(signalingManager:didDisconnectWithError:)])
    {
        dispatch_async(dispatch_get_main_queue(), ^
        {
            [self.delegate signalingManager:self didDisconnectWithError:error];
        });
    }
}

- (void)xmppStreamDidAuthenticate:(XMPPStream *)sender
{
    DDLogInfo(@"%s", __PRETTY_FUNCTION__);

    [self sendPresense:YES];

    if ([self.delegate respondsToSelector:@selector(signalingManager:didConnectWithError:)])
    {
        dispatch_async(dispatch_get_main_queue(), ^
        {
            [self.delegate signalingManager:self didConnectWithError:nil];
        });
    }
}

- (void)xmppStream:(XMPPStream *)sender didNotAuthenticate:(DDXMLElement *)errorElement
{
    DDLogInfo(@"%s", __PRETTY_FUNCTION__);

    if ([self.delegate respondsToSelector:@selector(signalingManager:didConnectWithError:)])
    {
        NSError *error = [NSError errorWithDomain:VSSIGNALINGMANAGER_ERROR_DOMAIN
                                             code:VSSignalingManagerError_AuthError
                                         userInfo:nil];
        dispatch_async(dispatch_get_main_queue(), ^
        {
            [self.delegate signalingManager:self didConnectWithError:error];
        });
    }
}

- (void)xmppStream:(XMPPStream *)sender didReceivePresence:(XMPPPresence *)presence
{
    DDLogInfo(@"%s - %@ is %@", __PRETTY_FUNCTION__, presence.from.user, presence.type);

    if ([presence.from isEqualToJID:sender.myJID options:XMPPJIDCompareUser])
    {
        return;
    }

    BOOL online = ![presence.type isEqualToString:PRESENCE_UNAVAILABLE];
    if ([self.delegate respondsToSelector:@selector(signalingManager:didReceivePresenceFromJID:isOnline:)])
    {
        dispatch_async(dispatch_get_main_queue(), ^
        {
            [self.delegate signalingManager:self didReceivePresenceFromJID:presence.from isOnline:online];
        });
    }
}

#pragma mark - VSMediaManagerDelegate

- (void)mediaManager:(VSMediaManager *)mediaManager errorOccured:(NSError *)error
{
    DDLogInfo(@"%s", __PRETTY_FUNCTION__);
    DDLogError(@"%s - Error: %@", __PRETTY_FUNCTION__, error);
}

- (void)mediaManagerDidSetSessionDescription:(VSMediaManager *)mediaManager
{
    DDLogInfo(@"%s", __PRETTY_FUNCTION__);
}

- (void)mediaManagerICENegotioationCompleted:(VSMediaManager *)mediaManager
{
    DDLogInfo(@"%s", __PRETTY_FUNCTION__);

    [self sendSesionMessage];
}

- (void)mediaManagerSignalingClosed:(VSMediaManager *)mediaManager
{
    DDLogInfo(@"%s", __PRETTY_FUNCTION__);

    self.mediaManager = nil;
}

- (void)mediaManager:(VSMediaManager *)mediaManager didReceivedRemoteVideoTrack:(RTCVideoTrack *)videoTrack
{
    DDLogInfo(@"%s", __PRETTY_FUNCTION__);

    if ([self.delegate respondsToSelector:@selector(signalingManager:didReceiveRemoteVideoTrack:)])
    {
        [self.delegate signalingManager:self didReceiveRemoteVideoTrack:videoTrack];
    }
}

- (void)mediaManager:(VSMediaManager *)mediaManager didReceivedLocalVideoTrack:(RTCVideoTrack *)videoTrack
{
    DDLogInfo(@"%s", __PRETTY_FUNCTION__);

    if ([self.delegate respondsToSelector:@selector(signalingManager:didReceiveLocalVideoTrack:)])
    {
        [self.delegate signalingManager:self didReceiveLocalVideoTrack:videoTrack];
    }
}

#pragma mark - Private

- (void)sendPresense:(BOOL)isOnline
{
    DDLogInfo(@"%s - isOnline = %d", __PRETTY_FUNCTION__, isOnline);

    XMPPPresence *presence = isOnline ? [XMPPPresence presence] : [XMPPPresence presenceWithType:PRESENCE_UNAVAILABLE];
    [self.stream sendElement:presence];
}

- (void)notifyCallStateChanged
{
    if ([self.delegate respondsToSelector:@selector(signalingManager:callStateChanged:)])
    {
        [self.delegate signalingManager:self callStateChanged:self.currentCall];
    }
}

- (void)sendSesionMessage
{
    DDLogInfo(@"%s", __PRETTY_FUNCTION__);

    NSString *localSessionDescription = self.mediaManager.localSessionDescription;
    VSCall *call = self.currentCall;
    XMPPJID *jidOfContact = call.contact.jid;
    NSArray *candidates = self.mediaManager.iceCandidatesSDP;
    VSCallState state = call.state;

    if (state == VSCallState_Connected)
    {
        [self.jingle acceptSessionWithSid:call.sessionId
                                    JIDTo:jidOfContact
                   withSessionDescription:localSessionDescription
                      transportCandidates:candidates];
    }
    else if (state == VSCallState_Created)
    {
        NSString *sessionId = [self.jingle initiateSessionWithJIDTo:jidOfContact
                                             withSessionDescription:localSessionDescription
                                                transportCandidates:candidates];
        self.currentCall.sessionId = sessionId;
    }
    else if (state == VSCallState_Terminated || state == VSCallState_Rejected)
    {
        BOOL success = call.state == VSCallState_Terminated;
        [self.jingle terminateSessionWithSid:call.sessionId
                                          to:jidOfContact
                                 isInitiator:call.isInitiator
                                     success:success];
    }
}

@end
