//
//  LoginScreenViewController.m
//  VoipSample
//
//  Created by Dmitry Lyzlov on 18/02/16.
//  Copyright © 2016 Dmitry Lyzlov. All rights reserved.
//

#import "VSLoginScreenViewController.h"

#import <GIDSignInButton.h>

#import "VSContactListViewController.h"
#import "VSCircleImageView.h"
#import "VSSignalingManager.h"
#import "VSAccountManager.h"

NSString * const SEGUE_ON_ENTER = @"onLogin";

@interface VSLoginScreenViewController () < VSAccountManagerSignInDelegate >

@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *loadingIndicator;
@property (weak, nonatomic) IBOutlet GIDSignInButton *signInButton;
@property (weak, nonatomic) IBOutlet UIView *userView;
@property (weak, nonatomic) IBOutlet UILabel *userName;
@property (weak, nonatomic) IBOutlet VSCircleImageView *userImage;

@end

@implementation VSLoginScreenViewController

#pragma mark - Public

- (void)viewDidLoad
{
    [super viewDidLoad];

    [self.loadingIndicator startAnimating];

    VSAccountManager *accountManagerInstance = [VSAccountManager sharedInstance];
    accountManagerInstance.signInDelegate = self;
    [accountManagerInstance signInSilently];
}

- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}

#pragma mark - Actions

- (IBAction)signOutPresed
{
    [[VSAccountManager sharedInstance] signOut];
    [self updateUserView];
}

- (IBAction)enterPressed
{
    [self performSegueWithIdentifier:SEGUE_ON_ENTER sender:self];
}

#pragma mark - VSAccountManagerSignInDelegate

- (void)accountManagerWillSignIn:(VSAccountManager *)manager
{
    self.signInButton.hidden = YES;
    [self.loadingIndicator startAnimating];
}

- (void)accountManager:(VSAccountManager *)manager didSignedWithError:(NSError *)error
{
    // TODO: error handling
    [self updateUserView];
}

- (void)accountManager:(VSAccountManager *)manager requestsPresentViewController:(UIViewController *)viewController
{
    __weak VSLoginScreenViewController *weakSelf = self;
    [self presentViewController:viewController animated:YES completion:^
    {
        [weakSelf.loadingIndicator stopAnimating];
    }];
}

- (void)accountManager:(VSAccountManager *)manager requestsDismissViewController:(UIViewController *)viewController
{
    [self.loadingIndicator startAnimating];

    __weak VSLoginScreenViewController *weakSelf = self;
    [self dismissViewControllerAnimated:YES completion:^
    {
        [weakSelf updateUserView];
    }];
}

#pragma mark - Private

- (void)updateUserView
{
    [self.loadingIndicator stopAnimating];

    GIDProfileData *profile = [VSAccountManager sharedInstance].currentUserProfile;
    BOOL hasUser = (BOOL)profile;
    self.signInButton.hidden = hasUser;
    self.userView.hidden = !hasUser;

    self.userName.text = profile.name;
    self.userImage.url = [profile imageURLWithDimension:self.userImage.frame.size.width];
}

#pragma mark - Navigation

- (BOOL)shouldPerformSegueWithIdentifier:(NSString *)identifier sender:(id)sender
{
    return [identifier isEqualToString:SEGUE_ON_ENTER] && sender == self;
 }

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:SEGUE_ON_ENTER])
    {
        if ([segue.destinationViewController isKindOfClass:[UITabBarController class]])
        {
            VSAccountManager *accountManagerInstance = [VSAccountManager sharedInstance];
            NSArray *viewControllers = ((UITabBarController *)segue.destinationViewController).viewControllers;
            for (UIViewController *vc in viewControllers)
            {
                if ([vc isKindOfClass:[VSContactListViewController class]])
                {
                    VSContactListViewController *contactListVC = (VSContactListViewController *)vc;
                    contactListVC.contactListDataSource = accountManagerInstance;
                    accountManagerInstance.contactListDelegate = contactListVC;
                }
            }

            [accountManagerInstance connectToMessagingService];
        }
    }
}

@end
