//
//  VSAccountManager.h
//  VoipSample
//
//  Created by Dmitry Lyzlov on 27/02/16.
//  Copyright © 2016 Dmitry Lyzlov. All rights reserved.
//

#import <Foundation/Foundation.h>

#import <GIDProfileData.h>
#import "VSContactListDelegation.h"

@class VSAccountManager;
@class UIViewController;
@class VSCall;
@class RTCVideoTrack;

@protocol VSAccountManagerSignInDelegate < NSObject >

- (void)accountManagerWillSignIn:(VSAccountManager *)manager;
- (void)accountManager:(VSAccountManager *)manager didSignedWithError:(NSError *)error;
- (void)accountManager:(VSAccountManager *)manager requestsPresentViewController:(UIViewController *)viewController;
- (void)accountManager:(VSAccountManager *)manager requestsDismissViewController:(UIViewController *)viewController;

@end

@protocol VSAccountManagerCallDelegate < NSObject >

- (void)accountManager:(VSAccountManager *)manager callStateChanged:(VSCall *)call;
- (void)accountManager:(VSAccountManager *)manager didReceiveRemoteVideoTrack:(RTCVideoTrack *)videoTrack;
- (void)accountManager:(VSAccountManager *)manager didReceiveLocalVideoTrack:(RTCVideoTrack *)videoTrack;

@end

@interface VSAccountManager : NSObject < VSContactListDataSource >

@property (weak, nonatomic) id< VSAccountManagerSignInDelegate > signInDelegate;
@property (weak, nonatomic) id< VSAccountManagerCallDelegate > callDelegate;
@property (weak, nonatomic) id< VSContactListDelegate > contactListDelegate;

@property (strong, nonatomic, readonly) GIDProfileData *currentUserProfile;

+ (instancetype)sharedInstance;

- (void)signInSilently;
- (void)signOut;

- (void)connectToMessagingService;
- (void)disconnectFromMessagingService;

- (void)acceptCall;
- (void)rejectCall;

@end
