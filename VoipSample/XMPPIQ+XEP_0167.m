//
//  XMPPIQ+XEP_0167.m
//  VoipSample
//
//  Created by Dmitry Lyzlov on 28/02/16.
//  Copyright © 2016 Dmitry Lyzlov. All rights reserved.
//

#import "XMPPIQ+XEP_0167.h"

#import <NSXMLElement+XMPP.h>

#import "XMPPIQ+Utilities.h"

@implementation XMPPIQ (XEP_0167)

#pragma mark - Public

- (BOOL)isJingleMessage
{
    return [self elementsForLocalName:ELEMENT_JINGLE URI:XMLNS_JINGLE].firstObject != nil;
}

- (JingleAction)getAction
{
    JingleAction action = JingleAction_Unknown;
    NSString *actionString = [((NSXMLElement *)[self elementsForLocalName:ELEMENT_JINGLE URI:XMLNS_JINGLE].firstObject) attributeForName:ATTRIBUTE_ACTION].stringValue;
    if ([actionString isEqualToString:ATTRIBUTE_ACTION_VALUE_INITIATE])
    {
        action = JingleAction_SessionInitiate;
    }
    else if ([actionString isEqualToString:ATTRIBUTE_ACTION_VALUE_ACCEPT])
    {
        action = JingleAction_SessionAccept;
    }
    else if ([actionString isEqualToString:ATTRIBUTE_ACTION_VALUE_TERMINATE])
    {
        action = JingleAction_SessionTerminate;
    }
    else if ([actionString isEqualToString:ATTRIBUTE_ACTION_VALUE_INFO])
    {
        action = JingleAction_SessionInfo;
    }
    return action;
}

- (NSString *)getSid
{
    NSString *sid = [((NSXMLElement *)[self elementsForLocalName:ELEMENT_JINGLE URI:XMLNS_JINGLE].firstObject) attributeForName:ATTRIBUTE_SID].stringValue;
    return sid;
}

- (NSString *)getSDPString
{
    NSXMLElement *jingleElement = (NSXMLElement *)[self elementsForLocalName:ELEMENT_JINGLE URI:XMLNS_JINGLE].firstObject;
    NSXMLElement *contentElement = (NSXMLElement *)[jingleElement elementsForName:ELEMENT_CONTENT].firstObject;
    NSString *SDPString = ((NSXMLElement *)[contentElement elementsForLocalName:ELEMENT_DESCRIPTION URI:XMLNS_JINGLE_SDP].firstObject).stringValue;
    return SDPString;
}

- (NSXMLElement *)formatJinleElementWithAction:(JingleAction)action initiator:(XMPPJID *)initiator responder:(XMPPJID *)responder sessionId:(NSString *)sid
{
    /*
     <jingle xmlns='urn:xmpp:jingle:1'
             action='session-initiate'
             initiator='romeo@montague.lit/orchard'
             sid='a73sjjvkla37jfea'>
     
     <jingle xmlns='urn:xmpp:jingle:1'
             action='session-accept'
             initiator='romeo@montague.lit/orchard'
             responder='juliet@capulet.lit/balcony'
             sid='a73sjjvkla37jfea'>
     */
    NSXMLElement *jingle = [NSXMLElement elementWithName:ELEMENT_JINGLE URI:XMLNS_JINGLE];
    [jingle addAttributeWithName:ATTRIBUTE_ACTION stringValue:[self getActionStringByAction:action]];
    [jingle addAttributeWithName:ATTRIBUTE_SID stringValue:sid];

    if (initiator)
    {
        [jingle addAttributeWithName:ATTRIBUTE_INITIATOR stringValue:initiator.full];
    }

    if (responder)
    {
        [jingle addAttributeWithName:ATTRIBUTE_RESPONDER stringValue:responder.full];
    }

    return jingle;
}

- (NSXMLElement *)formatContentElement
{
    /*
     <content creator='initiator' name='voice'>
     */
    NSXMLElement *content = [NSXMLElement elementWithName:ELEMENT_CONTENT];
    [content addAttributeWithName:ATTRIBUTE_CREATOR stringValue:ATTRIBUTE_INITIATOR];
    [content addAttributeWithName:ATTRIBUTE_NAME stringValue:ATTRIBUTE_CONTENT_NAME_VALUE_AUDIO];
    return content;
}

- (NSXMLElement *)formatDescriptionElementWithType:(DescriptionMediaType)type codecsArray:(NSArray *)codecs
{
    /*
     <description xmlns='urn:xmpp:jingle:apps:rtp:1' media='audio'>
         <payload-type id='96' name='speex' clockrate='16000'/>
     */
    NSXMLElement *description = [NSXMLElement elementWithName:ELEMENT_DESCRIPTION URI:XMLNS_JINGLE_RTP];
    [description addAttributeWithName:ATTRIBUTE_MEDIA stringValue:[self getMediaTypeStringByDescriptionMediaType:type]];
    for (NSDictionary *codec in codecs)
    {
        NSXMLElement *payload = [self formatPayloadFromDisctionary:codec];
        [description addChild:payload];
    }
    return description;
}

- (NSXMLElement *)formatDescriptionElementWithSDPString:(NSString *)SDPString
{
    /*
     <description xmlns='urn:xmpp:jingle:apps:sdp'>
         m=audio 1 RTP/SAVPF 111 103 104 0 8 107 106 105 13 126
         c=IN IP4 0.0.0.0
         a=rtcp:1 IN IP4 0.0.0.0
         a=ice-options:google-ice
         a=extmap:1 urn:ietf:params:rtp-hdrext:ssrc-audio-level
         a=sendrecv
         a=mid:audio
         a=rtcp-mux
         a=crypto:1 AES_CM_128_HMAC_SHA1_80 inline:+dMXm95m1recm8SlC3Ux66LH+z7Ve1LKoPKjbxWL
         a=rtpmap:111 opus/48000/2
         a=fmtp:111 minptime=10
         a=rtpmap:103 ISAC/16000
         a=rtpmap:104 ISAC/32000
         a=rtpmap:0 PCMU/8000
         a=rtpmap:8 PCMA/8000
         a=rtpmap:107 CN/48000
         a=rtpmap:106 CN/32000
         a=rtpmap:105 CN/16000
         a=rtpmap:13 CN/8000
         a=rtpmap:126 telephone-event/8000
         a=maxptime:60
         a=ssrc:3176601530 cname:GD84ngCycPaY3cQx
         a=ssrc:3176601530 msid:QoHel4kmL4ZFaJuTwmz3VpyxzMRCcNDEmcCl QoHel4kmL4ZFaJuTwmz3VpyxzMRCcNDEmcCla0
         a=ssrc:3176601530 mslabel:QoHel4kmL4ZFaJuTwmz3VpyxzMRCcNDEmcCl
         a=ssrc:3176601530 label:QoHel4kmL4ZFaJuTwmz3VpyxzMRCcNDEmcCla0
     </description>
     */

    NSXMLElement *description = [NSXMLElement elementWithName:ELEMENT_DESCRIPTION URI:XMLNS_JINGLE_SDP];
    [description setStringValue:SDPString];

    return description;
}

// TODO: move to XEP_0176

//- (NSXMLElement *)formatTransportElementForRAWUDPwithCandidates:(NSArray *)candidates
//{
//    /*
//     <transport xmlns='urn:xmpp:jingle:transports:raw-udp:1'>
//         <candidate component='1'
//             generation='0'
//             id='a9j3mnbtu1'
//             ip='10.1.1.104'
//             port='13540'/>
//     </transport>
//     */
//
//    NSXMLElement *transport = [NSXMLElement elementWithName:ELEMENT_TRANSPORT URI:XMLNS_JINGLE_TRANSPORT_UDP_RAW];
//    NSArray *candidateKeys = @[ ATTRIBUTE_ID, ATTRIBUTE_GENERATION, ATTRIBUTE_IP, ATTRIBUTE_PORT ];
//    for (NSDictionary *item in candidates)
//    {
//        NSXMLElement *candidate = [NSXMLElement elementWithName:ELEMENT_CANDIDATE];
//        [self fillElement:candidate withValuesFromDictionary:item byKeys:candidateKeys];
//        [transport addChild:candidate];
//    }
//    return transport;
//}

- (NSXMLElement *)formatRingingElement
{
    /*
     <ringing xmlns='urn:xmpp:jingle:apps:rtp:1:info'/>
     */
    NSXMLElement *ringing = [NSXMLElement elementWithName:ELEMENT_RINGING URI:XMLNS_JINGLE_RTP_RINGING];
    return ringing;
}

- (NSXMLElement *)formatReasonElementWithSuccess:(BOOL)success
{
    /*
     <reason>
         <success/>
         <text>I&apos;m outta here!</text>
     </reason>
     
     <reason>
         <busy/>
     </reason>
     */

    NSXMLElement *reason = [NSXMLElement elementWithName:ELEMENT_REASON];
    NSString *successElementName = success ? ELEMENT_SUCCESS : ELEMENT_BUSY;
    NSXMLElement *successElement = [NSXMLElement elementWithName:successElementName];
    [reason addChild:successElement];
    return reason;
}

#pragma mark - Private

- (NSString *)getActionStringByAction:(JingleAction)action
{
    NSString *actionString = @"unknown";
    switch (action)
    {
        case JingleAction_SessionInitiate:
            actionString = ATTRIBUTE_ACTION_VALUE_INITIATE;
            break;
        case JingleAction_SessionAccept:
            actionString = ATTRIBUTE_ACTION_VALUE_ACCEPT;
            break;
        case JingleAction_SessionTerminate:
            actionString = ATTRIBUTE_ACTION_VALUE_TERMINATE;
            break;
        case JingleAction_SessionInfo:
            actionString = ATTRIBUTE_ACTION_VALUE_INFO;
            break;
        default:
            break;
    }
    return actionString;
}

- (NSString *)getMediaTypeStringByDescriptionMediaType:(DescriptionMediaType)type
{
    return type == DescriptionMediaType_Video ? @"video" : @"media";
}

- (NSXMLElement *)formatPayloadFromDisctionary:(NSDictionary *)payloadDisctionary
{
    /*
     <payload-type id='96' name='speex' clockrate='16000' ptime='40'>
         <parameter name='vbr' value='on'/>
         <parameter name='cng' value='on'/>
     </payload-type>
     */

    NSXMLElement *payload = [NSXMLElement elementWithName:ELEMENT_PAYLOAD_TYPE];

    NSArray *payloadAttributes = @[ ATTRIBUTE_ID, ATTRIBUTE_NAME, ATTRIBUTE_CLOCKRATE, ATTRIBUTE_PTIME, ATTRIBUTE_CHANNELS, ATTRIBUTE_MAXPTIME ];
    [self fillElement:payload withValuesFromDictionary:payloadDisctionary byKeys:payloadAttributes];

    NSArray *specificParameters = payloadDisctionary[ELEMENT_PARAMETER];
    NSArray *specificParameterKeys = @[ ATTRIBUTE_NAME, ATTRIBUTE_VALUE ];
    for (NSDictionary *item in specificParameters)
    {
        NSXMLElement *parameter = [NSXMLElement elementWithName:ELEMENT_PARAMETER];
        [self fillElement:parameter withValuesFromDictionary:item byKeys:specificParameterKeys];
        [payload addChild:parameter];
    }

    return payload;
}

#pragma mark - SDP



@end
