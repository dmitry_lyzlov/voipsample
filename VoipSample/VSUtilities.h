//
//  VSUtilities.h
//  VoipSample
//
//  Created by Dmitry Lyzlov on 29/02/16.
//  Copyright © 2016 Dmitry Lyzlov. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface VSUtilities : NSObject

+ (NSString *)getIPAddress:(BOOL)preferIPv4;

@end
