//
//  VSCall.m
//  VoipSample
//
//  Created by Dmitry Lyzlov on 03/03/16.
//  Copyright © 2016 Dmitry Lyzlov. All rights reserved.
//

#import "VSCall.h"

@interface VSCall()

@property (strong, nonatomic) VSContact *contact;
@property (assign, nonatomic) BOOL isInitiator;

@end

@implementation VSCall

- (instancetype)initWithContact:(VSContact *)contact isInitiator:(BOOL)isInitiator
{
    self = [super init];
    if (self)
    {
        self.state = VSCallState_Unknown;
        self.contact = contact;
        self.isInitiator = isInitiator;
    }

    return self;
}

@end
