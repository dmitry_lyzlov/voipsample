//
//  XMPPIQ+XEP_0176.m
//  VoipSample
//
//  Created by Dmitry Lyzlov on 05/03/16.
//  Copyright © 2016 Dmitry Lyzlov. All rights reserved.
//

#import "XMPPIQ+XEP_0176.h"

#import <NSXMLElement+XMPP.h>

#import "XMPPIQ+Utilities.h"

@implementation XMPPIQ (XEP_0176)

#pragma mark - Public

- (NSXMLElement *)formatTransportElementWithICEUDPData:(NSDictionary *)data candidates:(NSArray *)candidates
{
    /*
     <transport xmlns='urn:xmpp:jingle:transports:ice-udp:1'
         pwd='asd88fgpdd777uzjYhagZg'
         ufrag='8hhy'>
         <candidate component='1'
             foundation='1'
             generation='0'
             id='el0747fg11'
             ip='10.0.1.1'
             network='1'
             port='8998'
             priority='2130706431'
             protocol='udp'
             type='host'/>
         <candidate component='1'
             foundation='2'
             generation='0'
             id='y3s2b30v3r'
             ip='192.0.2.3'
             network='1'
             port='45664'
             priority='1694498815'
             protocol='udp'
             rel-addr='10.0.1.1'
             rel-port='8998'
             type='srflx'/>
     </transport>
     */

    NSXMLElement *transport = [NSXMLElement elementWithName:ELEMENT_TRANSPORT URI:XMLNS_JINGLE_TRANSPORT_UDP_ICE];
    [self fillElement:transport withValuesFromDictionary:data byKeys:@[ ATTRIBUTE_PWD, ATTRIBUTE_UFRAG ]];
    for (NSString *item in candidates)
    {
        NSXMLElement *candidate = [self formatCandidateElementWithSDPString:item];
        [transport addChild:candidate];
    }
    return transport;
}

- (NSArray *)getCandidates
{
    NSMutableArray *candidates = [NSMutableArray array];

    NSXMLElement *jingleElement = (NSXMLElement *)[self elementsForLocalName:ELEMENT_JINGLE URI:XMLNS_JINGLE].firstObject;
    NSXMLElement *contentElement = (NSXMLElement *)[jingleElement elementsForName:ELEMENT_CONTENT].firstObject;
    NSXMLElement *transportElement = (NSXMLElement *)[contentElement elementsForLocalName:ELEMENT_TRANSPORT URI:XMLNS_JINGLE_TRANSPORT_UDP_ICE].firstObject;

    NSString *ufrag = [transportElement attributeStringValueForName:ATTRIBUTE_UFRAG];
    NSArray *candidateElements = [transportElement elementsForName:ELEMENT_TRANSPORT];
    for (NSXMLElement *item in candidateElements)
    {
        NSString *candidateSDPString = [self formatSDPStringFromCandidateElement:item];
        candidateSDPString = [NSString stringWithFormat:@"%@ ufrag %@", candidateSDPString, ufrag];
        [candidates addObject:candidateSDPString];
    }

    return candidates;
}

#pragma mark - Private

- (NSXMLElement *)formatCandidateElementWithSDPString:(NSString *)SDPString
{
    /*
     <candidate component='1'
         foundation='1'
         generation='0'
         id='el0747fg11'
         ip='10.0.1.1'
         network='1'
         port='8998'
         priority='2130706431'
         protocol='udp'
         type='host'/>
     */

    NSXMLElement *candidate = [NSXMLElement elementWithName:ELEMENT_CANDIDATE];
    NSDictionary *data = [[self class] extractICECandidateDataFromSDPString:SDPString];
    NSArray *candidateAttributes = @[ ATTRIBUTE_FOUNDATION,
                                      ATTRIBUTE_COMPONENT,
                                      ATTRIBUTE_PROTOCOL,
                                      ATTRIBUTE_PRIORITY,
                                      ATTRIBUTE_IP,
                                      ATTRIBUTE_PORT,
                                      ATTRIBUTE_TYPE,
                                      ATTRIBUTE_GENERATION,
                                      ATTRIBUTE_REL_ADDR,
                                      ATTRIBUTE_REL_PORT ];
    [self fillElement:candidate withValuesFromDictionary:data byKeys:candidateAttributes];
    return candidate;
}

- (NSString *)formatSDPStringFromCandidateElement:(NSXMLElement *)candidateElement
{
    /*
     candidate:2608808550 1 udp 2122260223 192.168.1.33 61678 typ host generation 0 ufrag JwfNB+YFLnOJwx0I,
     candidate:2608808550 2 udp 2122260222 192.168.1.33 59971 typ host generation 0 ufrag JwfNB+YFLnOJwx0I,
     candidate:1836006322 2 udp 1686052606 95.37.196.92 59971 typ srflx raddr 192.168.1.33 rport 59971 generation 0 ufrag JwfNB+YFLnOJwx0I,
     candidate:1836006322 1 udp 1686052607 95.37.196.92 61678 typ srflx raddr 192.168.1.33 rport 61678 generation 0 ufrag JwfNB+YFLnOJwx0I,
     candidate:3590110870 1 tcp 1518280447 192.168.1.33 62960 typ host tcptype passive generation 0 ufrag JwfNB+YFLnOJwx0I,
     candidate:3590110870 2 tcp 1518280446 192.168.1.33 62961 typ host tcptype passive generation 0 ufrag JwfNB+YFLnOJwx0I
     */

    NSDictionary *elementData = candidateElement.attributesAsDictionary;
    NSString *SDPString = [NSString stringWithFormat:@"candidate:%@ %@ %@ %@ %@ %@ typ %@ generation %@",
                           elementData[ATTRIBUTE_FOUNDATION],
                           elementData[ATTRIBUTE_COMPONENT],
                           elementData[ATTRIBUTE_PROTOCOL],
                           elementData[ATTRIBUTE_PRIORITY],
                           elementData[ATTRIBUTE_IP],
                           elementData[ATTRIBUTE_PORT],
                           elementData[ATTRIBUTE_TYPE],
                           elementData[ATTRIBUTE_GENERATION]];
    NSString *value = nil;
    value = elementData[ATTRIBUTE_REL_ADDR];
    if (value.length > 0)
    {
        SDPString = [NSString stringWithFormat:@"%@ raddr %@", SDPString, value];
    }

    value = elementData[ATTRIBUTE_REL_PORT];
    if (value.length > 0)
    {
        SDPString = [NSString stringWithFormat:@"%@ rport %@", SDPString, value];
    }

    return SDPString;
}

@end
