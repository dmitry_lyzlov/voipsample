//
//  VSContactTableViewCell.h
//  VoipSample
//
//  Created by Dmitry Lyzlov on 21/02/16.
//  Copyright © 2016 Dmitry Lyzlov. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "VSContactListDelegation.h"

@class VSContactListCell;
@class VSContact;

@protocol VSContactListCellDelegate < NSObject >

- (void)cell:(VSContactListCell *)cell didSelectedAction:(VSContactListAction)action;

@end


@interface VSContactListCell : UITableViewCell

@property (weak, nonatomic) id <VSContactListCellDelegate > actionDelegate;
@property (strong, nonatomic) VSContact *contactInfo;

@end
