//
//  VSCircleImageView.h
//  VoipSample
//
//  Created by Dmitry Lyzlov on 22/02/16.
//  Copyright © 2016 Dmitry Lyzlov. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface VSCircleImageView : UIImageView

@property (strong, nonatomic) NSURL *url;
@property (strong, nonatomic) NSString *urlString;

@end
