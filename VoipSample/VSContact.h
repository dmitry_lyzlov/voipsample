//
//  VSContact.h
//  VoipSample
//
//  Created by Dmitry Lyzlov on 29/02/16.
//  Copyright © 2016 Dmitry Lyzlov. All rights reserved.
//

#import <Foundation/Foundation.h>

@class XMPPJID;

@interface VSContact : NSObject

@property (strong, nonatomic, readonly) XMPPJID *jid;
@property (strong, nonatomic, readonly) NSString *name;
@property (strong, nonatomic, readonly) NSString *image;

- (instancetype)initWithJID:(XMPPJID *)jid;

- (BOOL)isEqualToContact:(VSContact *)contact;
- (BOOL)isEqualToJID:(XMPPJID *)jid;

@end
