//
//  VSCallViewController.m
//  VoipSample
//
//  Created by Dmitry Lyzlov on 02/03/16.
//  Copyright © 2016 Dmitry Lyzlov. All rights reserved.
//

#import "VSCallViewController.h"

#import "VSAccountManager.h"
#import "VSCall.h"
#import "VSContact.h"
#import "VSVideoCallView.h"

@interface VSCallViewController() < VSAccountManagerCallDelegate >

@property (weak, nonatomic) IBOutlet UILabel *callerNameLabel;
@property (weak, nonatomic) IBOutlet UIButton *acceptButton;
@property (weak, nonatomic) IBOutlet VSVideoCallView *videoCallView;
@property (weak, nonatomic) IBOutlet UILabel *stateLabel;

@end

@implementation VSCallViewController

#pragma mark - Public

- (void)viewDidLoad
{
    [VSAccountManager sharedInstance].callDelegate = self;
    self.callerNameLabel.text = nil;
    self.acceptButton.hidden = !self.acceptButtonAvailable;
}

#pragma mark - Actions

- (IBAction)onAcceptPressed:(UIButton *)sender
{
    sender.hidden = YES;
    [[VSAccountManager sharedInstance] acceptCall];
}

- (IBAction)onDeclinePressed:(UIButton *)sender
{
    sender.hidden = YES;
    [[VSAccountManager sharedInstance] rejectCall];
}

#pragma mark - VSAccountManagerCallDelegate

- (void)accountManager:(VSAccountManager *)manager callStateChanged:(VSCall *)call
{
    UILabel *callerNameLabel = self.callerNameLabel;
    if (!callerNameLabel.text)
    {
        callerNameLabel.text = call.contact.name;
    }

    VSCallState state = call.state;
    if (state == VSCallState_Connected)
    {
        self.videoCallView.hidden = NO;
    }

    self.stateLabel.text = [self getStateStringByCallState:state];
}

- (void)accountManager:(VSAccountManager *)manager didReceiveRemoteVideoTrack:(RTCVideoTrack *)videoTrack
{
    [self.videoCallView setRemoteVideoTrack:videoTrack];
}

- (void)accountManager:(VSAccountManager *)manager didReceiveLocalVideoTrack:(RTCVideoTrack *)videoTrack
{
    // TODO: implement
}

#pragma mark - Private

- (NSString *)getStateStringByCallState:(VSCallState)state
{
    NSString *stateString = nil;

    switch (state)
    {
        case VSCallState_Created:
            stateString = @"Connecting...";
            break;
        case VSCallState_Received:
            stateString = @"Incoming call";
            break;
        case VSCallState_Connected:
            stateString = @"Connected";
            break;
        case VSCallState_Ringing:
            stateString = @"Ringing...";
            break;
        case VSCallState_Rejected:
            stateString = @"Call rejected";
            break;
        case VSCallState_Terminated:
            stateString = @"Disconnected";
            break;
        default:
            break;
    }

    return stateString;
}

@end
