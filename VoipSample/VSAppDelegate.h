//
//  AppDelegate.h
//  VoipSample
//
//  Created by Dmitry Lyzlov on 18/02/16.
//  Copyright © 2016 Dmitry Lyzlov. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface VSAppDelegate : UIResponder < UIApplicationDelegate >

@property (strong, nonatomic) UIWindow *window;

@end

