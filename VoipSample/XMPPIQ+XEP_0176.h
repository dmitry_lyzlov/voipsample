//
//  XMPPIQ+XEP_0176.h
//  VoipSample
//
//  Created by Dmitry Lyzlov on 05/03/16.
//  Copyright © 2016 Dmitry Lyzlov. All rights reserved.
//

#import <XMPPIQ.h>

@interface XMPPIQ (XEP_0176)

- (NSXMLElement *)formatTransportElementWithICEUDPData:(NSDictionary *)data candidates:(NSArray *)candidates;
- (NSArray *)getCandidates;

@end
