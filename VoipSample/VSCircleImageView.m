//
//  VSCircleImageView.m
//  VoipSample
//
//  Created by Dmitry Lyzlov on 22/02/16.
//  Copyright © 2016 Dmitry Lyzlov. All rights reserved.
//

#import "VSCircleImageView.h"

@implementation VSCircleImageView

#pragma mark - Properties

- (void)setUrlString:(NSString *)urlString
{
    _urlString = urlString;
    self.url = [NSURL URLWithString:urlString];
}

- (void)setUrl:(NSURL *)url
{
    _url = url;
    if (!url)
    {
        self.image = nil;
    }
    else
    {
        [self downloadImage];
    }
}

#pragma mark - Public

- (void)awakeFromNib
{
    self.layer.cornerRadius = self.frame.size.width / 2;
    self.clipsToBounds = YES;
}

#pragma mark - Private

- (void)downloadImage
{
    __weak VSCircleImageView *weakSelf = self;
    dispatch_async(dispatch_get_global_queue(QOS_CLASS_USER_INITIATED, 0), ^
    {
        NSData *data = [NSData dataWithContentsOfURL:weakSelf.url];
        UIImage *image = [UIImage imageWithData:data];
        dispatch_async(dispatch_get_main_queue(), ^
        {
            weakSelf.image = image;
        });
    });
}

@end
