//
//  AppDelegate.m
//  VoipSample
//
//  Created by Dmitry Lyzlov on 18/02/16.
//  Copyright © 2016 Dmitry Lyzlov. All rights reserved.
//

#import "VSAppDelegate.h"

#import <Google/SignIn.h>
#import <CocoaLumberJack/DDTTYLogger.h>
#import <RTCPeerConnectionFactory.h>
#import <RTCLogging.h>

#import "VSCall.h"
#import "VSCallViewController.h"

//static const int xmppLogLevel = XMPP_LOG_LEVEL_INFO | XMPP_LOG_FLAG_TRACE;

@interface VSAppDelegate ()

@end

@implementation VSAppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
#if TARGET_IPHONE_SIMULATOR
    setenv("XcodeColors", "YES", 1);
//    [DDLog addLogger:[DDTTYLogger sharedInstance] withLogLevel:xmppLogLevel];
    [DDLog addLogger:[DDTTYLogger sharedInstance]];
    [[DDTTYLogger sharedInstance] setColorsEnabled:YES];
#endif

    RTCSetMinDebugLogLevel(kRTCLoggingSeverityVerbose);
    [RTCPeerConnectionFactory initializeSSL];

    NSError* configureError;
    [[GGLContext sharedInstance] configureWithError:&configureError];
    NSAssert(!configureError, @"Error configuring Google services: %@", configureError);

    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(handleCallInitated:)
                                                 name:VS_NOTIFICATION_CALL_STATE_CHANGED
                                               object:nil];

    return YES;
}

- (BOOL)application:(UIApplication *)app openURL:(NSURL *)url options:(NSDictionary<NSString *,id> *)options
{
    return [[GIDSignIn sharedInstance] handleURL:url
                               sourceApplication:options[UIApplicationOpenURLOptionsSourceApplicationKey]
                                      annotation:options[UIApplicationOpenURLOptionsAnnotationKey]];
}

- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    [RTCPeerConnectionFactory deinitializeSSL];
}

#pragma mark - Notifications

- (void)handleCallInitated:(NSNotification *)notification
{
    VSCall *call = notification.object;
    VSCallState state = call.state;
    UINavigationController *navigationVC = (UINavigationController *)self.window.rootViewController;

    if (state == VSCallState_Created || state == VSCallState_Received)
    {
        VSCallViewController *callVC = [navigationVC.storyboard instantiateViewControllerWithIdentifier:NSStringFromClass([VSCallViewController class])];
        callVC.acceptButtonAvailable = state == VSCallState_Received;
        [navigationVC presentViewController:callVC animated:YES completion:nil];
    }
    else if ((state == VSCallState_Terminated || state == VSCallState_Rejected) &&
             [navigationVC.topViewController.presentedViewController isKindOfClass:[VSCallViewController class]])
    {
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^
        {
            [navigationVC dismissViewControllerAnimated:YES completion:nil];
        });
    }
}


@end
